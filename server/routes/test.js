const Sequelize = require('sequelize');

// Option 1: Passing parameters separately
const sequelize = new Sequelize('surveyapp', 'postgres', 'surveyapp', {
    host: '116.203.190.27',
    dialect:'postgres' /* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
});
sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');

        sequelize.query("SELECT ST_AsGeoJSON (geom) FROM projekty.parcels LIMIT 1", { type: sequelize.QueryTypes.SELECT})
            .then(data => {
                console.log(data);
            })
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });
