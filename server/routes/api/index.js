const router = require("express").Router();
const request = require('request');
const {models} = require('../../models');

router.all("/", function (req, res) {
    res.json({
        status: true,
        message: "Info about api",
        user: req.user
    });
});

router.use("/projects", require("./projects"));
router.use("/admin", require("./admin"));
router.use("/user", require("./user"));

router.use("/logout", async function (req, res) {
    try {
        const user = await models.Users.update({token: ''}, {where: {id: req.user.id}});
        return res.status(200).json({
            status: true,
            message: 'User was logged out!'
        });
    } catch (e) {
        return res.status(400).json({
            status: false,
            message: e.message || e
        });
    }
});
module.exports = router;
