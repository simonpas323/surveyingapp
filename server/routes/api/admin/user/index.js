const router = require('express').Router();
const {models} = require('../../../../models');

const {SERVER_CODES, checkItem, pagination} = require('../../extra');
const MODEL_NAME = 'Users';
const TABLE_NAME = MODEL_NAME;

async function checkIten(req, res, next) {
    checkItem(MODEL_NAME, req, res, next);
}

router.put('/:id', checkIten, async function (req, res) {
    try {
        const updates = {
            ...req.body,
        };
        if (req.body.role) {
            req.body.role = parseInt(req.body.role);
            if (
                req.user.role === 1 && req[MODEL_NAME].id === req.user.id && req.body.role > 1 ||
                req[MODEL_NAME].id === req.user.id && req.body.role !== req[MODEL_NAME].role
            ) {
                throw 'Not able to change the access';
            }
        }
        // if (typeof updates.password !== 'undefined' && updates.password) updates.password = models.Users.generateHash(updates.password);
        await req[MODEL_NAME].update(updates);
        Object.assign(req[MODEL_NAME], updates);
        res.json({
            status: true,
            message: `${MODEL_NAME} was updated`,
            data: req[MODEL_NAME],
        });
    } catch (error) {
        console.log(error);
        res.status(SERVER_CODES.BAD_STATUS).json({
            status: false,
            error,
            statusCODE: SERVER_CODES.EDIT_ITEM_ERROR,
            message: `${MODEL_NAME} was not updated`,
            data: req.item,
        });
    }
});

router.delete('/:id', checkIten, async function (req, res) {
    try {
        if (req.user.id === req[MODEL_NAME].id) throw 'Can`t delete self!';
        await req[MODEL_NAME].destroy();
        res.json({
            status: true,
            message: 'Item was deleted',
            data: req[MODEL_NAME],
        });
    } catch (error) {
        res.status(SERVER_CODES.BAD_STATUS).json({
            status: false,
            error,
            statusCODE: SERVER_CODES.DELETE_ITEM_ERROR,
            message: `${MODEL_NAME} was not deleted`,
            data: req.item,
        });
    }
});

router.post("", async function (req, res) {
    try {
        const link = req.protocol + '://' + req.get('host');
        const item = await models[MODEL_NAME].register({
            ...req.body,
            userId: req.user.id
        }, {link});
        res.json(item);
    } catch (error) {
        res.status(400).json({
            statusCODE: SERVER_CODES.CREATE_ERROR,
            error,
            message: `Not able to create ${MODEL_NAME}`
        });
    }

});

router.get('', function (req, res) {
    pagination({TABLE_NAME}, req, res);

});

router.get("/about", async function (req, res) {
    res.json({
        status: true,
        data: req.user,
    });
});

module.exports = router;
