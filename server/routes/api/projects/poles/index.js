const router = require('express').Router();
const {models} = require('../../../../models');
const {SERVER_CODES, checkItem, pagination} = require('../../extra');

async function checkIten(req, res, next) {
    checkItem('Poles', req, res, next);
}

const TABLE_NAME = 'Poles';
router.get('/:id', checkIten, function (req, res) {
    res.json({
        status: true,
        data: req.item,
    });

});

router.put('/:id', checkIten, async function (req, res) {
    try {
        await req.item.update({
            ...req.body,
            projectId: req.Projects.id
        });
        res.json({
            status: true,
            message: 'Item was updated',
            data: req.item,
        });
    } catch (error) {
        res.status(SERVER_CODES.BAD_STATUS).json({
            status: false,
            error,
            statusCODE: SERVER_CODES.EDIT_ITEM_ERROR,
            message: 'Item was not updated',
            data: req.item,
        });
    }
});

router.delete('/:id', checkIten, async function (req, res) {
    try {
        await req.item.destroy();
        res.json({
            status: true,
            message: 'Item was deleted',
            data: req.item,
        });
    } catch (error) {
        res.status(SERVER_CODES.BAD_STATUS).json({
            status: false,
            error,
            statusCODE: SERVER_CODES.DELETE_ITEM_ERROR,
            message: 'Item was not deleted',
            data: req.item,
        });
    }
});

router.post("", async function (req, res) {
    try {
        const item = await models.Poles.create({
            ...req.body,
            userId: req.user.id,
            projectId: req.Projects.id
        });
        res.json(item);
    } catch (error) {
        res.status(400).json({
            statusCODE: SERVER_CODES.CREATE_ERROR,
            error,
            message: 'Not able to create Pole'
        });
    }

});

router.get("", async function (req, res) {

    pagination({TABLE_NAME}, req, res);
});

module.exports = router;
