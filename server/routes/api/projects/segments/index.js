const router = require('express').Router();
const {models} = require('../../../../models');

const {SERVER_CODES, checkItem, pagination} = require('../../extra');
const MAP_ELEMENT = 'Segments';
const TABLE_NAME = MAP_ELEMENT;

async function checkIten(req, res, next) {
    checkItem(MAP_ELEMENT, req, res, next);
}

router.get('/:id', checkIten, function (req, res) {
    res.json({
        status: true,
        data: req.item,
    });

});

router.put('/:id', checkIten, async function (req, res) {
    try {
        await req.item.update({
            ...req.body,
        });
        res.json({
            status: true,
            message: 'Item was updated',
            data: req.item,
        });
    } catch (error) {
        res.status(SERVER_CODES.BAD_STATUS).json({
            status: false,
            error,
            statusCODE: SERVER_CODES.EDIT_ITEM_ERROR,
            message: 'Item was not updated',
            data: req.item,
        });
    }
});

router.delete('/:id', checkIten, async function (req, res) {
    try {
        await req.item.destroy();
        res.json({
            status: true,
            message: 'Item was deleted',
            data: req.item,
        });
    } catch (error) {
        res.status(SERVER_CODES.BAD_STATUS).json({
            status: false,
            error,
            statusCODE: SERVER_CODES.DELETE_ITEM_ERROR,
            message: 'Item was not deleted',
            data: req.item,
        });
    }
});

router.post("", async function (req, res) {
    try {
        // if (!req.body.powerLineid) throw 'Power Line is required';
        if (!req.body.poles) throw 'Poles are required';
        const item = await models.Segments.create({
            ...req.body,
            userId: req.user.id
        });
        // await item.addPoleSegments(req.body.poles);
        // await item.addSegmentPowerline([req.body.powerLineid]);
        item.dataValues.poles = await item.getPoleSegments().map((el) => el.id);
        res.json(item);
    } catch (error) {
        console.log(error);
        res.status(400).json({
            statusCODE: SERVER_CODES.CREATE_ERROR,
            error,
            message: 'Not able to create Segment'
        });
    }

});

router.get("", async function (req, res) {


    pagination({TABLE_NAME}, req, res);
});

module.exports = router;
