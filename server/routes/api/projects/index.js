const router = require('express').Router();
const {models} = require('../../../models');

const {SERVER_CODES, checkItem} = require('../extra');
const MAP_SCHEMA = 'Projects';
async function checkIten(req, res, next) {
    checkItem(MAP_SCHEMA, req, res, next);
}
async function checkItemId(req, res, next) {
    checkItem(MAP_SCHEMA, req, res, next,'projectId');
}


router.use("/:projectId/poles", checkItemId, require("./poles"));
router.use("/:projectId/parcels", checkItemId, require("./parcels"));
router.use("/:projectId/segments", checkItemId, require("./segments"));
router.use("/:projectId/stations", checkItemId, require("./stations"));
router.use("/:projectId/poi", checkItemId, require("./poi"));

router.get('/:id', checkIten, function (req, res) {
    res.json({
        status: true,
        data: req.item,
    });

});

router.put('/:id', checkIten, async function (req, res) {
    try {
        await req.item.update({
            ...req.body,
        });
        res.json({
            status: true,
            message: 'Item was updated',
            data: req.item,
        });
    } catch (error) {
        res.status(SERVER_CODES.BAD_STATUS).json({
            status: false,
            error,
            statusCODE: SERVER_CODES.EDIT_ITEM_ERROR,
            message: 'Item was not updated',
            data: req.item,
        });
    }
});

router.delete('/:id', checkIten, async function (req, res) {
    try {
        await req.item.destroy();
        res.json({
            status: true,
            message: 'Item was deleted',
            data: req.item,
        });
    } catch (error) {
        res.status(SERVER_CODES.BAD_STATUS).json({
            status: false,
            error,
            statusCODE: SERVER_CODES.DELETE_ITEM_ERROR,
            message: 'Item was not deleted',
            data: req.item,
        });
    }
});

router.post("", async function (req, res) {
    try {
        const item = await models[MAP_SCHEMA].create({
            ...req.body,
            userId: req.user.id
        });
        res.json(item);
    } catch (error) {
        console.log(error);
        res.status(400).json({
            statusCODE: SERVER_CODES.CREATE_ERROR,
            error,
            message: 'Not able to create Pole'
        });
    }

});

router.get("", async function (req, res) {
    models[MAP_SCHEMA].findAll({
        // order: 'createdAt DESC',
        limit: req.query.limit || 10,
        offset: req.query.offset || 0,
    }).then((result) => {
        res.json(result);
    }).catch((error) => {
        console.log(error);
        res.status(400).json({
            statusCODE: SERVER_CODES.FETCH_ERROR,
            error,
            message: 'Not able to fetch Poles'
        })
    })
});

module.exports = router;
