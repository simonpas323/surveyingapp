const SERVER_CODES = {
    BAD_STATUS: 400,
    FETCH_ERROR: 1,
    CREATE_ERROR: 2,
    GET_ITEM_ERROR: 3,
    EDIT_ITEM_ERROR: 4,
    DELETE_ITEM_ERROR: 5,
};
const {models, Op} = require('../../models');

async function checkItem(modelName, req, res, next, paramId = 'id') {
    try {
        const item = await await models[modelName].findByPk(req.params[paramId]);
        if (!item) throw `No ${modelName} available!`;
        req[modelName] = req.item = item;
        next();
    } catch (error) {
        res.status(400).json({
            statusCODE: SERVER_CODES.GET_ITEM_ERROR,
            error,
            message: error.message || error
        });
    }
}

function pagination({TABLE_NAME}, req, res, opt) {
    let options = {};
    let filters = {};
    let order = [
        ['createdAt', 'DESC']
    ];
    let where = {};
    if (['Users', 'Logs'].indexOf(TABLE_NAME) > -1) {

    } else {
        where = {
            projectId: req.Projects.id,
        }
    }
    try {
        if (req.query.filter) {
            const filter = JSON.parse(req.query.filter);
            filter.forEach((a) => {
                if (a.columnName === 'id') {
                    filters[a.columnName] = {
                        [Op.lte]: a.value
                    }
                } else if (a.columnName.match('At')) {
                    filters[a.columnName] = {
                        [Op.lte]: a.value
                    }
                } else {
                    filters[a.columnName] = {
                        [Op.like]: `%${a.value}%`
                    }

                }
            });
        }
        if (req.query.sort) {
            try {
                const sort = JSON.parse(req.query.sort)[0];
                if (sort) order = [
                    [
                        sort.selector,
                        sort.desc ? 'DESC' : "ASC",

                    ]
                ];
            } catch (e) {
                console.log(e);
            }

        }
        if (opt) {
            options={
                ...options,
                ...opt
            }
        }
    } catch (e) {
        console.log(e);
    }

    models[TABLE_NAME].findAndCountAll({

        where: {
            ...where,
            ...filters
        },
        // order: 'createdAt DESC',
        limit: req.query.limit || 10,
        offset: req.query.offset || 0,
        order,
        ...options
    }).then((result) => {
        res.json(result);
    }).catch((error) => {
        console.log(error);
        res.status(400).json({
            statusCODE: SERVER_CODES.FETCH_ERROR,
            error,
            message: `Not table to fetch ${TABLE_NAME}`
        })
    })
}

module.exports = {
    SERVER_CODES,
    pagination,
    checkItem
}
