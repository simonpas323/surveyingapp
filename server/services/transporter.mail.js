const fs = require('fs');
const sgMail = require('@sendgrid/mail');
const path = require('path');
sgMail.setApiKey('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');
var Mail = {
    FROM: 'noreply@test.com',
    send: function (opt) {
        const self = this;
        return new Promise((resolve, reject) => {
            try {
                let content = '',
                    title = opt.title || 'Email confirmation!',
                    action = opt.action || 'Confirm my email',
                    _action = opt.action ? 'reset password' : 'verify email',
                    subject = '',
                    notifyAdmin;

                if (opt._type == 1) {
                    content = `<div >
<p  style="    color: #b8c0cb;font: 400 1rem/1.428 'Open Sans', sans-serif;">Hello <b>${opt.name}</b>. Welcome aboard to ${opt.link}. Use your email and next password <b>${opt.password}</b>  to get access </p>
<br/>
</div>`;
                } else {
                    content = `<div >
<p  style="    color: #b8c0cb;font: 400 1rem/1.428 'Open Sans', sans-serif;">Hello <b>${opt.name}</b>. Please click button bellow to ${_action}. </p>
<br/>
<p style="text-align: center"> <a style="  padding: 20px; background:#147eff;  color: white;font: 400 1rem/1.428 'Open Sans', sans-serif;" href="${opt.link}">${action}</a></p>
</div>`;
                }


                subject = opt.title || `Email confirmation`;

                let _recievers = opt.reciever.split(',').filter((el) => !el.match('@test.com')).join(',');
                console.log('-------------MAIL OPTIONS------');
                // console.log(recievers, subject, content);
                if (!content) return resolve('No content');
                console.log('-------------MAIL SEND------');

                let htmlContent = fs.readFileSync(path.resolve(__dirname, `./mail.templates/verify.email.html`), {encoding: 'utf8'});
                htmlContent = htmlContent.replace('@content@', content);
                htmlContent = htmlContent.replace('@reciever@', _recievers);
                htmlContent = htmlContent.replace('@title@', title);

                sgMail.send({
                    to: _recievers,
                    from: self.FROM,
                    subject: subject,
                    text: 'text information',
                    html: htmlContent,
                }, function (error, response) {
                    if (error) {
                        reject(error);
                        console.log('ERROR on sending mail, ', error);
                    } else {
                        console.log('mail was send, ');
                        resolve(error);
                    }

                });
            } catch (e) {
                console.log('EXEPTION on sending mail, ', e);
                reject(e);
            }

        });

    },
};
module.exports = Mail;
