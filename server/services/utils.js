const TWILIO_ACCOUNT = 'AC57ef5b319f17391abe6ea99cb752ba90';
const TWILIO_API_KEY = '563007dfc5919ddf4219d244ee80b06a';
const SEND_SMS_FROM = '+16503977171';
const TWILIO_GET_EVENT_STATUS_URL = 'https://tetrudpp2a.execute-api.us-west-1.amazonaws.com/v1/smseventhook';
const SENDGRID_API_KEY = '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';
var request = require('request');
var Q = require('q');
var fs = require('fs');
const path = require('path');


var sendmail = function(req, body) {
  return new Promise((resolve, reject) => {
    try {
      //Parse Request input
      var params;

      if (body.payload) {
        params = body.payload;
      } else {
        return reject('payload field not found on request body');
      }

      //Get fields

      var type = params.type;
      var from = params.from;
      var to = params.to;
      var subject = params.subject;
      var msgbody = params.body;
      var templateId = params.templateId;
      var templateArgs = params.templateArgs;

      var object_id = params.object_id;
      var object_type = params.object_type;

      //Create model
      var currdatetime = new Date();
      var model = {

        'type': type,
        'from': from,
        'to': to,
        'subject': subject,
        'body': msgbody,
        'templateId': templateId,
        'templateArgs': templateArgs,
        'dateCreated': currdatetime + '',
        'object_id': object_id,
        'object_type': object_type,

      };

      for (var key in params) {
        model[key] = params[key];

      }

      model.tenantId = req.tenantId;
      model.userId = req.userId;
      model.dateCreated = new Date().toISOString().slice(0, 19).replace('T', ' ');
      model.createdBy = req.userId;

      var mailreq = {};

      var recipientlist = [];

      if (typeof (model.to) === typeof ([])) {
        for (var recipient in model.to) {
          recipientlist.push({ 'email': model.to[recipient] });
        }

      } else {
        recipientlist.push({ 'email': model.to });
      }

      mailreq.personalizations = [{ 'to': recipientlist }];
      mailreq.from = { 'email': model.from };
      mailreq.subject = model.subject;
      mailreq.content = [{ 'type': 'text/html', 'value': model.body }];
      mailreq.tracking_settings = {
        'click_tracking': { 'enable': true, 'enable_text': true },
        'open_tracking': { 'enable': true },
        'subscription_tracking': { 'enable': true },
        'ganalytics': { 'enable': true },
      };

      //Attach template if present
      if (model.templateId) {
        mailreq.template_id = model.templateId;

      }

      //Attach substitutions if present
      if (model.templateArgs) {
        mailreq.personalizations[0].substitutions = model.templateArgs;
      }

      console.log(JSON.stringify(mailreq));

      for (var key in params) {
        if (model[key] === undefined) {
          model[key] = params[key];
        }
      }

      /**************** CUSTOM EMAIL TEMPLATES *******************************/

      verify_custom_template(params, req).then(function(templated_body) {
        if (templated_body !== null) {
          mailreq.content[0].value = templated_body;
          console.log(templated_body);
        }
        //Send request to sendgrid
        request({
          url: 'https://api.sendgrid.com/v3/mail/send',
          method: 'POST',
          headers: {
            'authorization': 'Bearer ' + SENDGRID_API_KEY,
            'content-type': 'application/json',
          },
          json: mailreq,
        }, function(error, response, respbody) {
          if (error || respbody) {

            model.errors = response.body.errors;
            model.request = body;
            model.status = 'Unsent';
            reject({ model });

          } else {
            model.message_id = response.headers['x-message-id'];
            model.status = 'Sent';
            resolve({ model });

          }
        });

      }).catch(reject);


    } catch (err) {
      reject({ error: err });
    }

  });

};
var verify_custom_template = function(params, req) {
  var deferred = Q.defer();


  var tenantName = params.tenantName = req.tenant.name;

  if (params['template-name'] === undefined)
    deferred.resolve(null);

  var templatefilename = '';
  switch (params['template-name']) {
    case 'approve-ppv':
      templatefilename = './EmailTemplates/template_approve-ppv.html';
      break;

    case 'price-help':
      templatefilename = './EmailTemplates/template_price-help.html';
      break;

    case 'price-exception':
      templatefilename = './EmailTemplates/template_price-help.html';
      break;

    case 'lead-exception':
      templatefilename = './EmailTemplates/template_lead-help.html';
      break;

    case 'approve-manufacturer':
      templatefilename = './EmailTemplates/template_approve_manufacturer.html';
      break;

    case 'approve-part':
      templatefilename = './EmailTemplates/template_approve_part_correction.html';
      break;


    default:
      deferred.resolve(null);

  }


  try {
    var contents = fs.readFileSync(path.resolve(__dirname, templatefilename), { encoding: 'utf8' });
    for (var key in params.templateArgs) {
      contents = contents.replace(new RegExp(key, 'g'), params.templateArgs[key]);
    }
  } catch (err) {
    console.log(err);
    deferred.resolve(null);
    contents = '';
  }


  request({
    url: 'https://3ccwwxf5o9.execute-api.us-west-1.amazonaws.com/v1' + '/getemaillinks',
    method: 'POST',
    headers: {
      'content-type': 'application/json',
    },
    json: {
      'payload': params,
    },
  }, function(error, response, respbody) {
    console.log(respbody, typeof (respbody));
    console.log(error);
    if (error) {
      console.log(error);
      console.log(respbody);
    } else {
      console.log(respbody, typeof (respbody));
      //respbody=JSON.parse(respbody);
      var links = respbody['links'];
      for (var key in links) {

        contents = contents.replace(new RegExp(key, 'g'), links[key]);
        console.log(key, links[key], contents);
      }
    }

    contents = contents.replace(new RegExp('{TenantName}', 'g'), tenantName);
    deferred.resolve(contents);
  });


  return deferred.promise;
};
//Helper function to send sms using twilio
var sendsms = function(req, body) {
  return new Promise((resolve, reject) => {
    try {
      //Parse Request input
      var params;

      if (body.payload) {
        params = body.payload;
      } else {
        reject('payload field not found on request body');
        return;
      }

      //Get fields

      var type = params.type;
      var from = params.from;
      var to = params.to;
      var subject = params.subject;
      var msgbody = params.body;
      var templateId = params.templateId;
      var templateArgs = params.templateArgs;

      //Create model
      var currdatetime = new Date();
      var model = {

        'type': type,
        'from': from,
        'to': to,
        'subject': subject,
        'body': msgbody,
        'templateId': templateId,
        'templateArgs': templateArgs,
        'dateCreated': currdatetime + '',

      };

      for (var key in params) {
        model[key] = params[key];

      }

      model.tenantId = req.tenantId;
      model.userId = req.userId;
      model.dateCreated = new Date().toISOString().slice(0, 19).replace('T', ' ');
      model.createdBy = req.userId;

      for (var key in params) {
        if (model[key] === undefined) {
          model[key] = params[key];
        }
      }

      //Send message

      request.post({
        url: `https://api.twilio.com/2010-04-01/Accounts/${TWILIO_ACCOUNT}/Messages.json`,
        json: true,
        auth: {
          user: TWILIO_ACCOUNT,
          pass: TWILIO_API_KEY,
        },
        form: {
          From: SEND_SMS_FROM,
          To: model.to,
          Body: model.body,
          StatusCallback: TWILIO_GET_EVENT_STATUS_URL,
        },
      }, function(err, res, resbody) {

        //console.log(response);

        model.logs = resbody;

        var sentstatus = 'SMS sent.';
        if (resbody.hasOwnProperty('message')) {
          sentstatus = resbody.message;
          model.status = 'Unsent';
        } else if (resbody.hasOwnProperty('error_message') && resbody.error_message !== null && resbody.error_message !== '') {
          sentstatus = resbody.error_message;
          model.status = 'Unsent';
        } else {
          model.status = 'Sent';
        }
        console.log(sentstatus);
        resolve({ model, sentstatus });
      });

    } catch (err) {
      console.log(err);
      reject({ error: err });
    }
  });

};


module.exports = {
  sendsms,
  sendmail,
};
