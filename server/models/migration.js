const {sequelize, models} = require('./index');
const {PREFIX} = require('./config');

const copySql = `
INSERT INTO ${PREFIX}.parcels (  userId, projectId,points)
 SELECT 1 ,1 , geom  as points
FROM projekty.parcels LIMIT 1
;

`;
/*sequelize.query(copySql).then((res) => {
    console.log(res);
}).catch((e) => {
    console.log(e);
});*/

const count = 1000000000;
const list = [
    {
        from: `Select  kontrahent as contractor, nazwa as title, id   from projekty.zlecenia `,
        toName: 'projects',
        to: models.Projects
    },
    {
        from: `Select * , ST_AsGeoJSON(ST_Transform(geom,4326)) As points from projekty.parcels   LIMIT ${count}`,
        toName: 'parcels',
        to: models.Parcels
    },
    {
        from: `Select *, ST_AsGeoJSON(ST_Transform(geom,4326)) As points from projekty.linie_zadania_gotowe   LIMIT ${count}`,
        toName: 'segments',
        to: models.Segments
    },
    {
        from: `Select * ,  ST_AsGeoJSON(ST_Transform(geom,4326)) As points from projekty.slupy   LIMIT ${count}`,
        toName: 'poles',
        to: models.Poles
    },
    {
        from: `Select * ,  ST_AsGeoJSON(ST_Transform(geom,4326)) As points from projekty.stacje   LIMIT ${count}`,
        toName: 'stations',
        to: models.Stations
    }
];

function copyEls(list) {
    if (!list.length) return;
    const _sql = list.shift();
    sequelize.query(_sql.from).then(async (res) => {
        res = res[0];
        if (_sql.toName === 'projects') {
            // let sql = `INSERT INTO ${PREFIX}.${_sql.toName} ( id, contractor, title,userId ) VALUES`;
            // for (let i = 0; i < res.length; i++) {
            //     sql += `(${res[i].id},${res[i].contractor},${res[i].title},1),`;
            //
            // }
            // sql = sql.substr(0, sql.length - 1);
            // return sequelize.query(sql).then(()=> copyEls(list)).catch((er)=>{
            //     console.log(er);
            // })

            for (let i = 0; i < res.length; i++) {

                await _sql.to.create({
                    ...res[i],
                    userId: 1,
                });
            }
        } else {
            for (let i = 0; i < res.length; i++) {
                const rowItem = {
                    ...res[i]
                };
                rowItem.id = null;
                delete rowItem.id;
                await _sql.to.create({
                    ...rowItem,
                    points: res[i].points,
                    userId: 1,
                    projectId: res[i].ID_ZLECENIA || 3449
                })
            }
        }

        copyEls(list);
    }).catch((e) => {
        console.log(e);
    });
}

copyEls(list);
