const {PREFIX} = require('./config');
const PoleSegments = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const PoleSegments = sequelize.define(`segment_pole`, {
        poleId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        segmentId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.ACTIVE
        },
    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX

    });

    PoleSegments.associate = models => {
        models.Poles.belongsToMany(models.Segments, {through: PoleSegments,as: 'PoleSegments', foreignKey: 'poleId'});
        models.Segments.belongsToMany(models.Poles, {through: PoleSegments,as: 'PoleSegments', foreignKey: 'segmentId'});
    };
    PoleSegments.__SETTINGS = __SETTINGS;

    return PoleSegments;
}

module.exports = PoleSegments;
