const {PREFIX} = require('./config');
const Powerlines = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const Powerlines = sequelize.define(`powerline`, {

        title: DataTypes.STRING,
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.ACTIVE
        },

        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Powerlines.associate = models => {
        Powerlines.belongsTo(models.Users, {foreignKey: 'userId'});
    };

    Powerlines.__SETTINGS = __SETTINGS;

    return Powerlines;
}

module.exports = Powerlines;
