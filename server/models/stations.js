const {PREFIX} = require('./config');
const Stations = (sequelize, DataTypes) => {
    const __SETTINGS = {
        TYPE: {
            SIMPLE: 1
        },
        STATUS: {
            ACTIVE: 1
        },
    };
    const Stations = sequelize.define(`stations`, {

        title: DataTypes.STRING,
        description: DataTypes.STRING,
        nazw_stac: DataTypes.STRING,
        num_eksp_s: DataTypes.STRING,

        type: DataTypes.INTEGER,
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.ACTIVE
        },

        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        projectId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        points: DataTypes.GEOMETRY,

    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Stations.associate = models => {
        Stations.belongsTo(models.Users, {foreignKey: 'userId'});
        Stations.belongsTo(models.Projects, {foreignKey: 'projectId'});
    };
    Stations.__SETTINGS = __SETTINGS;

    return Stations;
}

module.exports = Stations;
