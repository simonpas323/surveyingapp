const {PREFIX} = require('./config');
const Segments = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const Segments = sequelize.define(`segments`, {

        title: DataTypes.STRING,
        nazwa_linii: DataTypes.STRING,
        nazwa_ciagu_id: DataTypes.STRING,
        NAZWA_TAB: DataTypes.STRING,

        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.ACTIVE
        },

        projectId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        points: DataTypes.GEOMETRY,

    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Segments.associate = models => {
        Segments.belongsTo(models.Users, {foreignKey: 'userId'});
        Segments.belongsTo(models.Projects, {foreignKey: 'projectId'});
    };

    Segments.__SETTINGS = __SETTINGS;

    return Segments;
}

module.exports = Segments;
