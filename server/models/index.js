const Sequelize = require('sequelize');
const config = require('../config');
const {PREFIX} = require('./config');
const Op = Sequelize.Op

const sequelize = new Sequelize(`postgres://${config.db.postgress.DB_USER}:${config.db.postgress.DB_PSW}@${config.db.postgress.DB_HOST}:5432/${config.db.postgress.DB_NAME}`);
// sequelize.query(
//         `
//         CREATE EXTENSION postgis ;`
//         );
// sequelize.createSchema(PREFIX);

const models = {
    Users: sequelize.import('./user'),
    Projects: sequelize.import('./projects'),
    Poles: sequelize.import('./poles'),
    Segments: sequelize.import('./segments'),
    Stations: sequelize.import('./stations'),
    Poi: sequelize.import('./poi'),
    Logs: sequelize.import('./logs'),
    // Powerlines: sequelize.import('./powerlines'),
    // PoleSegments: sequelize.import('./pole_segments'),
    // SegmentPowerline: sequelize.import('./segment_powerline'),
    Parcels: sequelize.import('./parcels'),
};

Object.keys(models).forEach(key => {
    if ('associate' in models[key]) {
        models[key].associate(models);
    }
});

module.exports = {
    Op,
    models,
    sequelize
};
