const {PREFIX} = require('./config');
const poi = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const Poi = sequelize.define(`poi`, {

        title: DataTypes.STRING,
        description: DataTypes.STRING,
        points: DataTypes.GEOMETRY,

        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        projectId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },


    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Poi.associate = models => {
        Poi.belongsTo(models.Users, {foreignKey: 'userId'});
        Poi.belongsTo(models.Projects, {foreignKey: 'projectId'});
    };

    Poi.__SETTINGS = __SETTINGS;

    return Poi;
}

module.exports = poi;
