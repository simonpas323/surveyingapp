const {PREFIX} = require('./config');
const parcels = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const Parcels = sequelize.define(`parcels`, {

        title: DataTypes.STRING,
        wojewodztw: DataTypes.STRING,
        gmina: DataTypes.STRING,
        points: DataTypes.GEOMETRY,

        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        projectId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },


    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Parcels.associate = models => {
        Parcels.belongsTo(models.Users, {foreignKey: 'userId'});
        Parcels.belongsTo(models.Projects, {foreignKey: 'projectId'});
    };

    Parcels.__SETTINGS = __SETTINGS;

    return Parcels;
}

module.exports = parcels;
