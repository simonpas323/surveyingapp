const {PREFIX} = require('./config');
const poles = (sequelize, DataTypes) => {
    const __SETTINGS = {
        TYPE: {
            SIMPLE: 1
        },
        STATUS: {
            ACTIVE: 1
        },
    };
    const Poles = sequelize.define(`poles`, {

        title: DataTypes.STRING,
        description: DataTypes.STRING,
        type: DataTypes.INTEGER,
        num_slup: DataTypes.STRING,
        layer: DataTypes.STRING,

        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.ACTIVE
        },

        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        projectId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        points: DataTypes.GEOMETRY,

    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    Poles.associate = models => {
        Poles.belongsTo(models.Users, {foreignKey: 'userId'});
        Poles.belongsTo(models.Projects, {foreignKey: 'projectId'});
    };
    Poles.__SETTINGS = __SETTINGS;

    return Poles;
}

module.exports = poles;
