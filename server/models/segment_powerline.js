const {PREFIX} = require('./config');
const SegmentPowerline = (sequelize, DataTypes) => {
    const __SETTINGS = {
        STATUS: {
            ACTIVE: 1
        },
    };
    const SegmentPowerline = sequelize.define(`segment_powerline`, {
        powerlineId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        segmentId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },

        userId: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        status: {
            type: DataTypes.INTEGER,
            defaultValue: __SETTINGS.STATUS.ACTIVE
        },
    }, {
        timestamps: true,
        paranoid: true,
        schema: PREFIX
    });

    SegmentPowerline.associate = models => {
        models.Powerlines.belongsToMany(models.Segments, {
            through: SegmentPowerline,
            as: 'SegmentPowerline',
            foreignKey: 'powerlineId'
        });
        models.Segments.belongsToMany(models.Powerlines, {
            through: SegmentPowerline,
            as: 'SegmentPowerline',
            foreignKey: 'segmentId'
        });
    };
    SegmentPowerline.__SETTINGS = __SETTINGS;

    return SegmentPowerline;
}

module.exports = SegmentPowerline;
