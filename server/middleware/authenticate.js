const passport = require('passport');
const {models} = require('../models');

/*module.exports = function(req, res, next) {
  var token = req.headers['authorization'];

  passport.authenticate('jwt', { session: true }, function(err, user, info) {
    if (err) {
      return res.status(401).json({
        status: false,
        message: err,
      });
    }

    if (!user) {
      return res.status(401).json({
        status: false,
        message: info.message,
      });
    }

    // if (!req.isAuthenticated()) {
    //   return res.status(401).json({
    //     status: false,
    //     message: "Not authenticated"
    //   });
    // }

    if (user.token !== token) {
      return res.status(401).json({
        status: false,
        message: 'Expired token',
      });
    }

    req.user = user;
    next();
  })(req, res, next);
};*/
module.exports = async function (req, res, next) {
    try {
        var token = req.headers['authorization'] || req.headers['Authorization'];
        if (!token) throw 'no token provided';
        const user = await models.Users.findOne({where: {token: token}});
        if (!user) throw 'Token is not valid';
        req.user = user;
        next();
    } catch (e) {
        return res.status(401).json({
            status: false,
            message: e.message || e,
        });
    }

};
