const Sequelize = require('sequelize');
const sequelize =  new Sequelize('postgres://surveyingapp:surveyingapp@localhost:5432/surveyingapp');
const config = require("../config");
sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });
module.exports = sequelize;
