const node_ssh = require('node-ssh');
const ssh = new node_ssh();

const config = {
    host: '116.203.190.27',
    port: '22',
    username: 'root',
    password: 'surveyapp'
};
deploy();

async function deploy() {
    try {
        ssh.connect(config).then(() => {
            ssh.execCommand(`cd /root/surveyingapp && git reset --hard && git checkout master && git pull origin master &&  npm install && npm run build  && pm2 restart 0`, {}).then(function (result) {
                console.log('STDOUT: ' + result.stdout);
                console.log('STDERR: ' + result.stderr)
                ssh.dispose();
            })
        });


    } catch (err) {
        console.log(err)
    }
}
