import React, {Component} from 'react';

import './index.scss';

import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';


export default class DeleteItemDialog extends Component<{ title: string, onCancel: Function, onAccept: Function }, { refreshing: boolean }> {

    state = {
        refreshing: false,
    };


    private onCancel = () => {
        this.props.onCancel();
    };
    private onAccept = () => {
        this.props.onAccept();
    };

    render() {
        return (
            <React.Fragment>
                <DialogTitle id="max-width-dialog-title">Are you sure to delete {this.props.title}</DialogTitle>

                <DialogActions>
                    <Button onClick={this.onCancel} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={this.onAccept}>
                        OK
                    </Button>
                </DialogActions>
            </React.Fragment>
        );
    }
}

