import React, {Component} from 'react';

import './index.scss';


export default function Title({children}: any) {
    return (
        <span title={children} className={'text-overflow'}>{children}</span>
    )
}
