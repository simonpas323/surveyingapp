import React, {Component} from 'react';

import * as PropTypes from 'prop-types';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import Grid from '@material-ui/core/Grid';


const _KeyboardDatePicker: any = KeyboardDatePicker;


const DateEditorBase = ({value, onValueChange}: any) => {
    const [selectedDate, setSelectedDate] = React.useState<Date | null>(
        null,
    );

    function handleDateChange(date: Date | null) {
        setSelectedDate(date);
        onValueChange(date ? new Date(date).toISOString() : null);
    }

    const handleChange = (event: any) => {
        const {value: targetValue} = event.target;
        if (targetValue.trim() === '') {
            onValueChange();
            return;
        }
        onValueChange(parseInt(targetValue, 10));
    };
    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <Grid container justify="space-around">
                <_KeyboardDatePicker
                    value={selectedDate}
                    onChange={handleDateChange}
                />
            </Grid>

        </MuiPickersUtilsProvider>
    );
};

DateEditorBase.propTypes = {
    value: PropTypes.number,
    onValueChange: PropTypes.func.isRequired,
    classes: PropTypes.object.isRequired,
};

DateEditorBase.defaultProps = {
    value: undefined,
};

export default DateEditorBase;
