import React, {Component} from 'react';
import './index.scss';

import {isSuperAdminSelector, signOut, userSelector} from '../../ducks/auth';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import Map from "../components/Map";
import ToolBar from "../components/ToolBar";
import Layout1 from "../components/Layouts/Layout1";
import {Redirect, Route, Switch} from "react-router";
import AdminPage from "../admin";
import HomePage from "../home";


class MainPage extends Component<{ signOut: any, user: any, isSuperAdmin: boolean },
    {}> {


    render() {
        return (
            <Layout1>
                <div className={'d-flex'}>
                    <Switch>
                        <Route path='/home' component={HomePage}/>
                        {
                            this.props.isSuperAdmin && (
                                <Route path='/admin' component={AdminPage}/>
                            )
                        }
                        <Route exact path="*" render={() => (
                            <Redirect to="/home"/>
                        )}/>
                    </Switch>
                </div>
            </Layout1>
        );
    }
}

const mapStateToProps = (state: any) => ({
    user: userSelector(state),
    isSuperAdmin: isSuperAdminSelector(state)
});
const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        signOut,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
