import React, {Component} from 'react';
import './index.scss';
import {
    changeContols,
    currentModeSelector,
    lastGeoPostionsSelector,
    locationParcelsSelector,
    locationPoisSelector,
    locationPolesSelector,
    locationSegmentsSelector,
    locationSelector,
    locationsSelector, locationStationsSelector,
    modesSelector,
    moduleName
} from "../../../../ducks/map";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Divider from '@material-ui/core/Divider';
import Checkbox from '@material-ui/core/Checkbox';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import {Parcel, Poi, Pole, Segment, Station} from "../../../../entities";
import StationIcon from "../../../../assets/img/station.svg";
import PoleIcon from "../../../../assets/img/pole.svg";
import PoiIcon from "../../../../assets/img/poi.svg";
import ParcelIcon from "../../../../assets/img/parcel.svg";
import SegmentIcon from "../../../../assets/img/segment.svg";
import ReactSVG from 'react-svg';
import Title from "../../../../components/title";

interface MapProjectsProps {


    pois: Array<Poi>,
    poles: Array<Pole>,
    segments: Array<Segment>,
    parcels: Array<Parcel>,
    stations: Array<Station>,
    changeContols: Function,
    showPois: boolean,
    showStations: boolean,
    showSegments: boolean,
    showParcels: boolean,
    showPoles: boolean
}

class Entities extends Component<MapProjectsProps,
    {}> {


    private handleChange = (name: string) => {
        return (e: any) => {
            this.props.changeContols({name, value: e.target.checked})
        }
    };

    render() {
        const {
            pois,
            poles,
            segments,
            parcels,
            stations,
            showPois,
            showStations,
            showSegments,
            showParcels,
            showPoles
        } = this.props;
        const props: any = this.props;

        const checkers: any = [
            {
                name: 'showPoles',
                label: (
                    <div className={'d-flex f-row a-c'}>
                                        <span
                                            className={`${!showPoles ? 'text-light' : ''}`}><Title>Electrical Pole ({poles.length})</Title></span>
                        <ReactSVG src={PoleIcon} className={'svg-icon'}/>
                    </div>
                )
            },
            {
                name: 'showSegments',
                label: (
                    <div className={'d-flex f-row a-c'}>
                                        <span
                                            className={`${!showSegments ? 'text-light' : ''}`}><Title>Powerline Segment ({segments.length})</Title></span>
                        <ReactSVG src={SegmentIcon} className={'svg-icon'}/>
                    </div>
                )
            },
            {
                name: 'showStations',
                label: (
                    <div className={'d-flex f-row a-c'}>
                                        <span
                                            className={`${!showStations ? 'text-light' : ''}`}><Title>Stations ({stations.length})</Title></span>
                        <ReactSVG src={StationIcon} className={'svg-icon'}/>
                    </div>
                )
            },
            {
                name: 'showParcels',
                label: (
                    <div className={'d-flex f-row a-c'}>
                                        <span
                                            className={`${!showParcels ? 'text-light' : ''}`}><Title>Land parcel ({parcels.length})</Title></span>
                        <ReactSVG src={ParcelIcon} className={'svg-icon'}/>
                    </div>
                )
            },
            {
                name: 'showPois',
                label: (
                    <div className={'d-flex f-row a-c'}>
                                        <span
                                            className={`${!showPois ? 'text-light' : ''}`}><Title>POI ({pois.length})</Title></span>
                        <ReactSVG src={PoiIcon} className={'svg-icon'}/>
                    </div>
                )
            },

        ];
        return (
            <React.Fragment>
                <div className={'d-flex f-col entities-info'}>
                    <span className={'text-light'}>Entities:</span>
                    <br/>
                    <div className={'d-flex f-col'}>
                        {
                            checkers.map((el: any) => {
                                return (
                                    <FormControlLabel
                                        key={el.name}
                                        control={
                                            <Checkbox
                                                checked={props[el.name]}
                                                onChange={this.handleChange(el.name)}
                                                value={el.name}
                                                color={'primary'}
                                                className={`my-checkbox ${props[el.name] ? 'checked' : ''}`}
                                                inputProps={{
                                                    'aria-label': 'primary checkbox',
                                                }}
                                            />
                                        }
                                        label={el.label}
                                    />
                                )
                            })
                        }


                    </div>
                </div>
                <Divider/>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        showPois: state[moduleName].showPois,
        showStations: state[moduleName].showStations,
        showSegments: state[moduleName].showSegments,
        showParcels: state[moduleName].showParcels,
        showPoles: state[moduleName].showPoles,
        pois: locationPoisSelector(state),
        segments: locationSegmentsSelector(state),
        poles: locationPolesSelector(state),
        parcels: locationParcelsSelector(state),
        stations: locationStationsSelector(state),
        error: state[moduleName].error,
    }
};

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        changeContols
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(Entities);
