import React, {Component} from 'react';
import './index.scss';
import {
    changeContols,
    currentModeSelector,
    lastGeoPostionsSelector, locationSelector,
    locationsSelector,
    modesSelector,
    moduleName
} from "../../../../ducks/map";
import {bindActionCreators} from "redux";
import {showDialogContent} from "../../../../ducks/dialogs";
import {fetchLocationPoi} from "../../../../ducks/map/poi";
import {fetchLocationParcels} from "../../../../ducks/map/parcels";
import {fetchLocationPoles} from "../../../../ducks/map/poles";
import {fetchLocationSegments} from "../../../../ducks/map/segments";
import {fetchLocations, selectLocation} from "../../../../ducks/map/locations";
import {fetchLocationStations} from "../../../../ducks/map/stations";
import {connect} from "react-redux";
import {Button} from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import Divider from '@material-ui/core/Divider';


interface MapProjectsProps {


    project: any,
    projects: Array<any>,
    showDialogContent: Function,
    fetchLocationSegments: Function,
    fetchLocationParcels: Function,
    fetchLocationPoi: Function,
    fetchLocationStations: Function,
    fetchLocationPoles: Function,
    selectLocation: Function,
    fetchLocations: Function
}

class Projects extends Component<MapProjectsProps,
    {}> {
    componentDidMount(): void {
        this.props.fetchLocations();
    }

    private onChange = async (proj: any) => {
        this.props.showDialogContent(null);
        const project = this.props.projects.filter((el: any) => el.id === proj.id)[0];
        this.props.selectLocation(project);
        this.loadProjectData(project);

    };
    private loadProjectData = async (project: any) => {
        await this.props.fetchLocationParcels(project);
        await this.props.fetchLocationPoles(project);
        await this.props.fetchLocationSegments(project);
        await this.props.fetchLocationStations(project);
        await this.props.fetchLocationPoi(project);
    };

    private openSelectProjectModal = () => {
        const {projects} = this.props;
        this.props.showDialogContent(
            <div className={'modal-container'}>
                <IconButton className={'close'} onClick={() => this.props.showDialogContent(null)}>
                    <Icon>close</Icon>
                </IconButton>
                <div className={'modal-title'}>Select project</div>
                <div className={'modal-body'}>
                    {
                        projects.map((el) => {
                            return <div className={'modal-option text-overflow'} title={el.title} key={el.id}
                                        onClick={() => this.onChange(el)}>{el.title}</div>
                        })
                    }
                </div>
            </div>
        )
    }

    render() {
        const {project} = this.props;
        if (project) {
            return (
                <React.Fragment>
                    <div className={'d-flex a-c j-a project-info'}>
                        <span className={'text-light'}>Project</span>
                        <span className={'selected-project text-overflow'} title={project.title}>{project.title}</span>
                        <IconButton className={'close'} onClick={this.openSelectProjectModal}>
                            <Icon>edit</Icon>
                        </IconButton>


                    </div>
                    <Divider/>
                </React.Fragment>
            )
        }
        return (
            <Button variant="contained" className={'my-btn btn-primary'} onClick={this.openSelectProjectModal}>
                SELECT PROJECT
            </Button>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        projects: locationsSelector(state),
        project: locationSelector(state),
        error: state[moduleName].error,
    }
};

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
        fetchLocationPoi,
        fetchLocationParcels,
        fetchLocationPoles,
        fetchLocationSegments,
        selectLocation,
        fetchLocationStations,
        fetchLocations,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(Projects);
