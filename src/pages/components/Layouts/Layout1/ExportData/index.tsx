import React from 'react';
import {exportToCsv} from "../../../../../utils";
import {isSuperAdminSelector, signOut, userSelector} from "../../../../../ducks/auth";
import {
    locationParcelsSelector,
    locationPoisSelector,
    locationPolesSelector,
    locationSegmentsSelector, locationStationsSelector, moduleName
} from "../../../../../ducks/map";
import {logSelector, usersSelector} from "../../../../../ducks/admin";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

import {Button, Icon} from '@material-ui/core';

function ExportData(props: any) {
    const {

        isSuperAdmin,
    }: any = props;


    function createCSVData(list: Array<any>, name: string) {
        let filename = '';
        let csvData: any = [];
        if (list.length) {
            filename = `${name}.csv`;
            const keys = Object.keys(list[0]);
            csvData = [
                keys,
                ...list.map((el: any) => {
                    return [
                        keys.map((key: any) => {
                            return el[key]
                        })
                    ]
                })
            ];
        }
        return {
            filename,
            csvData
        }
    }

    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    function handleClick(event: React.MouseEvent<HTMLButtonElement>) {
        setAnchorEl(event.currentTarget);
    }

    function handleClose() {
        setAnchorEl(null);
    }

    function exportTable(dataType: string) {
        const {filename, csvData} = createCSVData(props[dataType], dataType);
        exportToCsv(filename, csvData);
        handleClose();
    }

    const canExportLogs = location.href.match('admin/logs');
    const canExportUsers = location.href.match('admin/users');
    return (

        <div>
            <Button variant="contained" className={'my-btn btn-primary'} onClick={handleClick}>
                EXPORT DATA
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={() => exportTable('parcels')}>Parcels to CSV</MenuItem>
                <MenuItem onClick={() => exportTable('poles')}>Poles to CSV</MenuItem>
                <MenuItem onClick={() => exportTable('segments')}>Segments to CSV</MenuItem>
                <MenuItem onClick={() => exportTable('pois')}>Pois to CSV</MenuItem>
                <MenuItem onClick={() => exportTable('stations')}>Stations to CSV</MenuItem>
                {
                    isSuperAdmin && (
                        <React.Fragment>
                            {
                                canExportLogs && <MenuItem onClick={() => exportTable('logs')}>Logs to CSV</MenuItem>
                            }
                            {
                                canExportUsers && <MenuItem onClick={() => exportTable('users')}>Users to CSV</MenuItem>
                            }


                        </React.Fragment>
                    )
                }
            </Menu>
        </div>
    );
    // return (
    //     <CSVLink data={csvData} filename={filename} target="_blank">
    //         <Button variant="contained" className={'my-btn btn-primary'}>
    //             EXPORT DATA
    //         </Button>
    //     </CSVLink>
    // );
}

const mapStateToProps = (state: any) => ({
    polesList: state[moduleName].polesList,
    parcelList: state[moduleName].parcelList,
    stationList: state[moduleName].stationList,
    segmentList: state[moduleName].segmentList,
    poiList: state[moduleName].poiList,

    user: userSelector(state),
    isSuperAdmin: isSuperAdminSelector(state),
    segments: locationSegmentsSelector(state),
    parcels: locationParcelsSelector(state),
    pois: locationPoisSelector(state),
    poles: locationPolesSelector(state),
    stations: locationStationsSelector(state),
    logs: logSelector(state),
    users: usersSelector(state),
});
const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        signOut,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(ExportData);
