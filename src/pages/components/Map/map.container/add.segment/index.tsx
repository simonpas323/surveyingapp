import React, {Component} from 'react';
import './index.scss';
import {errorSelector, lastGeoPostionsSelector, locationSelector, changeContols} from "../../../../../ducks/map";
import {addPole} from "../../../../../ducks/map/poles";
import {addSegments, editSegments} from "../../../../../ducks/map/segments";
import {showDialogContent} from "../../../../../ducks/dialogs";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import {
    Pole,
    Segment,
    GPSCoordinate,
    Geometry,
    ParcelConductor
} from "../../../../../entities";
import {isSuperADMINAdminSelector, isSuperAdminSelector} from "../../../../../ducks/auth";


class AddSegmentDialog extends Component<{
    isAdmin: any,
    selectedItem: any,
    location: any,
    tempPosition: Array<any>,
    changeContols: Function,
    editItem: Function,
    onFinishEditItem: Function,
    addItem: Function,
    showDialogContent: Function
},
    { id: any }> {

    static defaultProps = {
        onFinishEditItem: () => 1
    }

    constructor(p: any) {
        super(p);
        this.state = {
            ...this.props.selectedItem
        }
    }

    private onChange = (e: any) => {
        const newState: any = {
            [e.target.name]: e.target.value
        };
        this.setState(newState);
    };


    private handleOk = async (e: any) => {
        try {
            if (this.state.id) {
                await this.props.editItem({
                    ...this.state,
                });
            } else {
                await this.props.addItem({
                    ...this.state,
                    poles: this.props.tempPosition.map((el: Pole) => el.id),
                    locationId: this.props.location.id
                });
            }
            this.props.onFinishEditItem();

        } catch (e) {
            console.log(e);
        }
        this.handleCancel(e);
    };

    private handleCancel = (e: any) => {
        this.props.showDialogContent(null);
    };


    render() {
        const {title, nazwa_ciagu_id, NAZWA_TAB, nazwa_linii}: any = this.state;
        const {isAdmin} = this.props;


        return (
            <React.Fragment>
                <DialogTitle id="max-width-dialog-title">View Segment</DialogTitle>
                <DialogContent>

                    <form autoComplete="off" className={'d-flex f-col'}>
                        <FormControl>
                            <TextField
                                label="Title"
                                value={title}
                                name={'title'}
                                onChange={this.onChange}
                                disabled={!isAdmin}
                                margin="normal"
                            />
                        </FormControl>
                        <FormControl>
                            <TextField
                                label="nazwa_ciagu_id"
                                value={nazwa_ciagu_id}
                                name={'nazwa_ciagu_id'}
                                onChange={this.onChange}
                                disabled={!isAdmin}
                                margin="normal"
                            />
                        </FormControl>
                        <FormControl>
                            <TextField
                                label="NAZWA_TAB"
                                value={NAZWA_TAB}
                                name={'NAZWA_TAB'}
                                onChange={this.onChange}
                                disabled={!isAdmin}
                                margin="normal"
                            />
                        </FormControl>
                        <FormControl>
                            <TextField
                                label="nazwa_linii"
                                value={nazwa_linii}
                                name={'nazwa_linii'}
                                onChange={this.onChange}
                                disabled={!isAdmin}
                                margin="normal"
                            />
                        </FormControl>


                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleCancel} color="primary">
                        Close
                    </Button>
                    {
                        isAdmin && (
                            <Button onClick={this.handleOk}>
                                Save
                            </Button>
                        )
                    }

                </DialogActions>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state: any) => ({
    error: errorSelector(state),
    location: locationSelector(state),
    isAdmin: isSuperADMINAdminSelector(state),
    tempPosition: lastGeoPostionsSelector(state)
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        changeContols,
        showDialogContent,
        editItem: editSegments,
        addItem: addSegments,
    }, dispatch)
);
const edit = connect(mapStateToProps, mapDispatchToProps)(AddSegmentDialog);
export default edit;
