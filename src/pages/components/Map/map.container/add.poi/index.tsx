import React, {Component} from 'react';
import './index.scss';
import {changeContols, errorSelector, locationSelector} from "../../../../../ducks/map";
import {addPoi, editPoi, removePoi} from "../../../../../ducks/map/poi";
import {showDialogContent} from "../../../../../ducks/dialogs";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import {Geometry, Poi} from "../../../../../entities";

import {
    moduleName
} from "../../../../../ducks/map";
import {isSuperADMINAdminSelector} from "../../../../../ducks/auth";

declare var google: any, InfoBox: any, M: any;

function hasErrors(fieldsError: any) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class AddPoiDialog extends Component<{
    selectedItem: Poi,
    isAdmin: any,
    location: any,
    position: any,
    onFinishEditItem: Function,
    addPoi: Function,
    editPoi: Function,
    removePoi: Function,
    changeContols: Function,
    showDialogContent: Function
},
    {}> {


    static defaultProps = {
        onFinishEditItem: () => false,
        position: new Geometry()
    };

    constructor(p: any) {
        super(p);
        this.state = {
            ...p.selectedItem
        };
    }

    private onChange = (e: any) => {
        const newState: any = {
            [e.target.name]: e.target.value
        };
        this.setState(newState);
    };


    private handleOk = async (e: any) => {
        try {
            const {title, type, description, id}: any = this.state;
            if (id) {
                this.props.editPoi({
                    ...this.state,
                });
                this.props.onFinishEditItem();
            } else {
                this.props.addPoi({
                    ...this.state,
                    points: this.props.position,
                    projectId: this.props.location.id
                });
                this.props.changeContols({
                    name: 'allowAddPoi',
                    value: false
                });
            }

        } catch (e) {
            console.log(e);
        }
        this.handleCancel(e);
    };
    private deleteItem = (e: any) => {
        try {
            this.props.removePoi({
                ...this.state,
            });
            this.props.onFinishEditItem();

        } catch (e) {
            console.log(e);
        }
        this.handleCancel(e);
    };

    private handleCancel = (e: any) => {
        this.props.showDialogContent(null);
    };


    render() {
        const {title, type, description, id}: any = this.state;
        const {isAdmin} = this.props;

        return (
            <React.Fragment>
                <DialogTitle id="max-width-dialog-title">{id ? 'Edit' : 'Add'} Point of interest</DialogTitle>
                <DialogContent>

                    <form autoComplete="off" className={'d-flex f-col'}>
                        {/* <FormControl>
                            <InputLabel htmlFor="age-simple">Type</InputLabel>
                            <Select
                                style={{minWidth: 120}}
                                value={type}
                                onChange={this.onChange}
                                inputProps={{
                                    name: 'type',
                                    id: 'type',
                                }}
                            >
                                {
                                    [
                                        {
                                            value: 1,
                                            text: 'Type1'
                                        },
                                        {
                                            value: 2,
                                            text: 'Type2'
                                        },
                                    ].map((el) => (<MenuItem key={el.value} value={el.value}>{el.text}</MenuItem>))
                                }
                            </Select>
                        </FormControl>
*/}
                        <FormControl>
                            <TextField
                                label="Title"
                                value={title}
                                name={'title'}
                                onChange={this.onChange}
                                disabled={!isAdmin}
                                margin="normal"
                            />
                        </FormControl>

                        <FormControl>
                            <TextField
                                rows={4}
                                multiline
                                label="Description"
                                value={description}
                                name={'description'}
                                onChange={this.onChange}
                                disabled={!isAdmin}
                                margin="normal"
                            />
                        </FormControl>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleCancel} color="primary">
                        Close
                    </Button>
                    {
                        isAdmin && (
                            <Button onClick={this.handleOk}>
                                Save
                            </Button>
                        )
                    }

                    {
                        id && (
                            <Button onClick={this.deleteItem}>
                                Delete
                            </Button>
                        )
                    }

                </DialogActions>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state: any) => ({
    allowAddPoi: state[moduleName].allowAddPoi,
    isAdmin: isSuperADMINAdminSelector(state),
    error: errorSelector(state),
    location: locationSelector(state),
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
        changeContols,
        editPoi,
        removePoi,
        addPoi,
    }, dispatch)
);
const edit = connect(mapStateToProps, mapDispatchToProps)(AddPoiDialog);
export default edit;
