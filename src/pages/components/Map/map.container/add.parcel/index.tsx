import React, {Component} from 'react';
import './index.scss';
import {errorSelector, lastGeoPostionsSelector, locationSelector, changeContols} from "../../../../../ducks/map";
import {addPole} from "../../../../../ducks/map/poles";
import {addPoleParcel, editParcel} from "../../../../../ducks/map/parcels";
import {showDialogContent} from "../../../../../ducks/dialogs";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import {
    Parcel,
    Pole,
    GPSCoordinate,
    Geometry,
    ParcelConductor
} from "../../../../../entities";
import {isSuperADMINAdminSelector} from "../../../../../ducks/auth";

declare var google: any, InfoBox: any, M: any;

function hasErrors(fieldsError: any) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class AddParcelDialog extends Component<{
    isAdmin: any,
    selectedItem: any,
    location: any,
    tempPosition: Array<any>,
    onFinishEditItem: Function,
    changeContols: Function,
    editItem: Function,
    addItem: Function,
    showDialogContent: Function
},
    {}> {


    static defaultProps: {
        onFinishEditItem: () => false
    }

    constructor(p: any) {
        super(p);
        this.state = {
            ...this.props.selectedItem
        }
    }

    private onChange = (e: any) => {
        const newState: any = {
            [e.target.name]: e.target.value
        };
        this.setState(newState);
    };


    private handleOk = async (e: any) => {
        try {
            await this.props.editItem({
                ...this.state,
            });
            this.props.onFinishEditItem({
                ...this.state,
            });
        } catch (e) {
            console.log(e);
        }
        this.handleCancel(e);
    };

    private handleCancel = (e: any) => {
        this.props.showDialogContent(null);
    };


    render() {
        const {title, type, description, wojewodztw, gmina}: any = this.state;


        const {isAdmin} = this.props;
        return (
            <React.Fragment>
                <DialogTitle id="max-width-dialog-title">View Parcel</DialogTitle>
                <DialogContent>

                    <form autoComplete="off" className={'d-flex f-col'}>
                        {/*    <FormControl>
                            <InputLabel htmlFor="age-simple">Type</InputLabel>
                            <Select
                                style={{minWidth: 120}}
                                value={type}
                                onChange={this.onChange}
                                inputProps={{
                                    name: 'type',
                                    id: 'type',
                                }}
                            >
                                {
                                    [
                                        {
                                            value: 1,
                                            text: 'Type1'
                                        },
                                        {
                                            value: 2,
                                            text: 'Type2'
                                        },
                                    ].map((el) => (<MenuItem key={el.value} value={el.value}>{el.text}</MenuItem>))
                                }
                            </Select>
                        </FormControl>*/}

                        <FormControl>
                            <TextField
                                label="gmina"
                                value={gmina}
                                name={'gmina'}
                                onChange={this.onChange}
                                disabled={!isAdmin}
                                margin="normal"
                            />
                        </FormControl>
                        <FormControl>
                            <TextField
                                label="wojewodztw"
                                value={wojewodztw}
                                name={'wojewodztw'}
                                onChange={this.onChange}
                                disabled={!isAdmin}
                                margin="normal"
                            />
                        </FormControl>
                        <FormControl>
                            <TextField
                                label="Title"
                                value={title}
                                name={'title'}
                                onChange={this.onChange}
                                disabled={!isAdmin}
                                margin="normal"
                            />
                        </FormControl>

                        {/* <FormControl>
                            <TextField
                                rows={4}
                                multiline
                                label="Description"
                                value={description}
                                name={'description'}
                                onChange={this.onChange}
                                margin="normal"
                            />
                        </FormControl>*/}
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleCancel} color="primary">
                        Close
                    </Button>
                    {
                        isAdmin &&(
                            <Button onClick={this.handleOk}>
                                Save
                            </Button>
                        )
                    }

                </DialogActions>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state: any) => ({
    error: errorSelector(state),
    location: locationSelector(state),
    isAdmin: isSuperADMINAdminSelector(state),
    tempPosition: lastGeoPostionsSelector(state)
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        changeContols,
        showDialogContent,
        addItem: addPoleParcel,
        editItem: editParcel,
    }, dispatch)
);
const edit = connect(mapStateToProps, mapDispatchToProps)(AddParcelDialog);
export default edit;
