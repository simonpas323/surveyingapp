import React, {Component} from 'react';
import './index.scss';
import {errorSelector, locationSelector} from "../../../../../ducks/map";
import {addPole, editPole} from "../../../../../ducks/map/poles";
import {showDialogContent} from "../../../../../ducks/dialogs";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {isSuperADMINAdminSelector, isSuperAdminSelector} from "../../../../../ducks/auth";


class AddPoleDialog extends Component<{
    location: any,
    isAdmin: any,
    selectedItem: any,
    addPole: Function,
    onFinishEditItem: Function,
    editItem: Function,
    showDialogContent: Function
},
    {}> {

    static defaultProps = {
        onFinishEditItem: () => 1
    }

    constructor(p: any) {
        super(p);
        this.state = {
            ...this.props.selectedItem
        }
    }

    private onChange = (e: any) => {
        const newState: any = {
            [e.target.name]: e.target.value
        };
        this.setState(newState);
    };


    private handleOk = (e: any) => {
        try {

            this.props.editItem({...this.state});
            this.props.onFinishEditItem();
        } catch (e) {
            console.log(e);
        }
        this.handleCancel(e);
    };

    private handleCancel = (e: any) => {
        this.props.showDialogContent(null);
    };


    render() {
        const {title, num_slup, description}: any = this.state;
        const {isAdmin} = this.props;

        return (
            <React.Fragment>
                <DialogTitle id="max-width-dialog-title">View Pole</DialogTitle>
                <DialogContent>

                    <form autoComplete="off" className={'d-flex f-col'}>
                        <FormControl>
                            <TextField
                                label="Title"
                                value={title}
                                name={'title'}
                                onChange={this.onChange}
                                disabled={!isAdmin}
                                margin="normal"
                            />
                        </FormControl>
                        <FormControl>
                            <TextField
                                label="num_slup"
                                value={num_slup}
                                name={'num_slup'}
                                onChange={this.onChange}
                                disabled={!isAdmin}
                                margin="normal"
                            />
                        </FormControl>

                        <FormControl>
                            <TextField
                                rows={4}
                                multiline
                                label="Description"
                                value={description}
                                name={'description'}
                                onChange={this.onChange}
                                disabled={!isAdmin}
                                margin="normal"
                            />
                        </FormControl>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleCancel} color="primary">
                        Close
                    </Button>
                    {
                        isAdmin &&(
                            <Button onClick={this.handleOk}>
                                Save
                            </Button>
                        )
                    }

                </DialogActions>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state: any) => ({
    error: errorSelector(state),
    isAdmin: isSuperADMINAdminSelector(state),
    location: locationSelector(state),
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
        addPole,
        editItem: editPole,
    }, dispatch)
);
const edit = connect(mapStateToProps, mapDispatchToProps)(AddPoleDialog);
export default edit;
