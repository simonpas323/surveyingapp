import React, {Component} from 'react';
import _ from 'lodash';
import './index.scss';
import {showAlert, showDialogContent} from "../../../../ducks/dialogs";
import {
    Parcel,
    Pole,
    GPSCoordinate,
    Geometry,
    SegmentConductor,
    ParcelConductor, Segment, Station, Poi
} from "../../../../entities";


import {
    locationSelector,
    changeContols,
    locationPolesSelector,
    modesSelector,
    lastGeoPostionsSelector,
    currentModeSelector,
    locationParcelsSelector,
    moduleName,
    locationSegmentsSelector,
    locationStationsSelector,
    locationPoisSelector
} from "../../../../ducks/map";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import AddPoiDialog from "./add.poi";
import AddParcelDialog from "./add.parcel";
import AddPoleDialog from "./add.pole";
import AddSegmentDialog from "./add.segment";
import AddStationDialog from "./add.station";
import {SETTINGS} from "../../../../entities/utils";
import {fetchLocationPoi} from "../../../../ducks/map/poi";
import {fetchLocationParcels} from "../../../../ducks/map/parcels";
import {fetchLocationPoles} from "../../../../ducks/map/poles";
import {fetchLocationSegments} from "../../../../ducks/map/segments";
import {fetchLocationStations} from "../../../../ducks/map/stations";
import {searchSelector} from "../../../../ducks/auth";


declare var google: any, createPopupClass: any, MarkerClusterer: any;

function createPolygon(path: any) {
    let polygon = new google.maps.Polygon({
        path: path,
        ...SETTINGS.ACTIVE.POLYGON
    });

    let lastPath: any = null,
        lastCenter: any = null;
    polygon.getPosition = function () {
        var path = this.getPath();
        if (lastPath == path) {
            return lastCenter;
        }
        lastPath = path;
        var bounds = new google.maps.LatLngBounds();
        path.forEach(function (latlng: any, i: any) {
            bounds.extend(latlng);
        });

        lastCenter = bounds.getCenter()
        return lastCenter;
    };
    return polygon;
}

function createPath(el: any) {

    let lastPath: any = null,
        lastCenter: any = null;
    el.getPosition = function () {
        var path = this.getPath();
        if (lastPath == path) {
            return lastCenter;
        }
        lastPath = path;
        var bounds = new google.maps.LatLngBounds();
        path.forEach(function (latlng: any, i: any) {
            bounds.extend(latlng);
        });

        lastCenter = bounds.getCenter()
        return lastCenter;
    };
    return el;
}

function createCircle(path: any) {
    let lastPath: any = null,
        lastCenter: any = null;
    path.getPosition = function () {
        const latlng = this.center;
        var path = latlng;
        if (lastPath == path) {
            return lastCenter;
        }
        lastPath = path;
        var bounds = new google.maps.LatLngBounds();
        bounds.extend(latlng);

        lastCenter = bounds.getCenter()
        return lastCenter;
    };
    return path;
}

function createMarker(path: any) {
    let lastPath: any = null,
        lastCenter: any = null;
    path.getPosition = function () {
        const latlng = this.position;
        var path = latlng;
        if (lastPath == path) {
            return lastCenter;
        }
        lastPath = path;
        var bounds = new google.maps.LatLngBounds();
        bounds.extend(latlng);

        lastCenter = bounds.getCenter()
        return lastCenter;
    };
    return path;
}

interface MapContainerProps {

    pois: Array<any>,
    parcels: Array<Parcel>,
    poles: Array<Pole>,
    segments: Array<Segment>,
    stations: Array<Station>,

    fetchLocationSegments: Function,
    fetchLocationParcels: Function,
    fetchLocationPoi: Function,
    fetchLocationStations: Function,
    fetchLocationPoles: Function,

    mapCenter: any,
    mapZoom: number,
    showPois: boolean,
    showStations: boolean,
    showSegments: boolean,
    showParcels: boolean,
    showPoles: boolean,
    location: any,
    showDialogContent: any,
    tempPosition: any,
    search: string,
    drawMode: string,
    changeContols: Function,
    drawModeList: Array<string>,
    parcelList: any,
    polesList: any,
    segmentList: any,
    stationList: any,
    poiList: any,
    dateFilter: any,
    allowAddPoi: any,
    showAlert: any
}

class MapContainer extends Component<MapContainerProps,
    {}> {
    private tempParcelConductor: ParcelConductor = new ParcelConductor({});
    private tempSegmentConductor: SegmentConductor = new SegmentConductor({});
    private drawedTempLine: Array<any> = [];
    private drawedStations: Array<any> = [];
    private drawedPois: Array<any> = [];
    private drawedPoles: Array<any> = [];
    private drawedClusters: Array<any> = [];
    private drawedParcels: Array<ParcelConductor> = [];
    private boundingBox: any;
    private drawedSegments: Array<SegmentConductor> = [];
    private propPoles: Array<Pole> = [];
    private propParcels: Array<Parcel> = [];
    private propSegments: Array<Segment> = [];
    private propStations: Array<Station> = [];
    private propPois: Array<Poi> = [];
    private segments: Array<any> = [];
    private parcelList: any;
    private polesList: any;
    private segmentList: any;
    private stationList: any;
    private poiList: any;
    private googleMap: any;
    private container: any;

    componentDidMount(): void {
        this.drawMap();
        // this.resetDrawMaPoles(this.props.poles, this.props.showPoles);
        const nextProps = this.props;
        this.resetDrawMapStations(nextProps.stations, nextProps.showStations, nextProps.search);
        this.resetDrawMapSegments(nextProps.segments, nextProps.showSegments, nextProps.search);
        this.resetDrawMaPoles(nextProps.poles, nextProps.showPoles, nextProps.search);
        this.resetDrawMapParcels(nextProps.parcels, nextProps.showParcels, nextProps.search);
        this.resetDrawMaPois(nextProps.pois, nextProps.showPois, nextProps.search);
    }

    shouldComponentUpdate(): boolean {
        return false;
    }

    componentWillReceiveProps(nextProps: MapContainerProps, nextContext: any): void {
        if (nextProps.stationList !== this.stationList || nextProps.showStations !== this.props.showStations) {
            this.resetDrawMapStations(nextProps.stations, nextProps.showStations, nextProps.search);
            this.stationList = nextProps.stationList;
        }
        if (nextProps.segmentList !== this.segmentList || nextProps.showSegments !== this.props.showSegments) {
            this.resetDrawMapSegments(nextProps.segments, nextProps.showSegments, nextProps.search);
            this.segmentList = nextProps.segmentList;
        }
        if (nextProps.polesList !== this.polesList || nextProps.showPoles !== this.props.showPoles) {
            this.resetDrawMaPoles(nextProps.poles, nextProps.showPoles, nextProps.search);
            this.polesList = nextProps.polesList;
        }
        if (nextProps.parcelList !== this.parcelList || nextProps.showParcels !== this.props.showParcels) {
            this.parcelList = nextProps.parcelList;
            this.resetDrawMapParcels(nextProps.parcels, nextProps.showParcels, nextProps.search);
        }
        if (nextProps.poiList !== this.poiList || nextProps.showPois !== this.props.showPois) {
            this.poiList = nextProps.poiList;
            this.resetDrawMaPois(nextProps.pois, nextProps.showPois, nextProps.search);
        }
        if (nextProps.dateFilter !== this.props.dateFilter || nextProps.search !== this.props.search) {
            this.resetDrawMapStations(nextProps.stations, nextProps.showStations, nextProps.search);
            this.resetDrawMapSegments(nextProps.segments, nextProps.showSegments, nextProps.search);
            this.resetDrawMaPoles(nextProps.poles, nextProps.showPoles, nextProps.search);
            this.resetDrawMapParcels(nextProps.parcels, nextProps.showParcels, nextProps.search);
            this.resetDrawMaPois(nextProps.pois, nextProps.showPois, nextProps.search);
        }
    }


    private setMapOnAll(map: any, markers: any) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    private resetDrawMapSegments(_list: Array<Segment>, show: boolean, search: string) {
        this.setMapOnAll(null, this.drawedSegments);
        this.drawedSegments = [];
        for (let i = 0; i < this.drawedClusters.length; i++) {
            if (this.drawedClusters[i].__type === 4) {
                this.drawedClusters.splice(i--, 1)[0].clearMarkers();
            }
        }

        if (!show || !_list.length) return;
        for (let i = 0, list: Array<any> = this.filterItems(_list, search); i < list.length; i++) {
            const el: any = list[i];
            for (let j = 0; j < el.points.coordinates.length; j++) {
                const pathList = [];
                const _points = el.points.coordinates[j];
                for (let k = 0; k < _points.length; k++) {
                    pathList.push(new GPSCoordinate(_points[k]))
                }
                const segmentPath = new google.maps.Polyline({
                    editable: false,
                    draggable: false,
                    clickable: true,
                    path: pathList,
                    geodesic: true,
                    ...SETTINGS.ACTIVE.LINE
                });
                this.drawedSegments.push(segmentPath);
                this.applyEvent(segmentPath, el);
                createPath(segmentPath);
            }
        }
        this.addCluster(4, this.drawedSegments);
    }

    private resetDrawMaPois(pois: Array<Poi>, show: boolean, search: string) {
        this.setMapOnAll(null, this.drawedPois);
        this.drawedPois = [];
        if (!show || !pois.length) return;
        const {drawMode, drawModeList, showDialogContent} = this.props;
        this.filterItems(pois, search).forEach((el: Poi, index) => {

            const position: GPSCoordinate = el.points.toGPS();
            /*const Popup = createPopupClass();
            const poleSettings = new Popup(
                new google.maps.LatLng(position.lng, position.lat),
                `
            <div class="d-flex f-col">
            <b>${el.title}</b>
            <span>${el.description}</span>
            </div>
`, el
            );
             poleSettings._onClick((ev: any, el: Poi) => {
                showDialogContent(
                    <AddPoiDialog
                        selectedItem={el}
                        position={new Geometry()}/>
                );
            });
            */
            const image = '/assets/img/poi.png?icon=1';
            const poleSettings = new google.maps.Marker({
                position: position,
                clickable: true,
                editable: false,
                draggable: false,
                icon: image
            });
            this.applyEvent(poleSettings, el);


            poleSettings.animation = google.maps.Animation.DROP;

            this.drawedPois.push(poleSettings);
        });
        this.setMapOnAll(this.googleMap, this.drawedPois);
    }

    private resetDrawMaPoles(poles: Array<Pole>, show: boolean, search: string) {
        this.setMapOnAll(null, this.drawedPoles);
        this.drawedPoles = [];
        for (let i = 0; i < this.drawedClusters.length; i++) {
            if (this.drawedClusters[i].__type === 3) {
                this.drawedClusters.splice(i--, 1)[0].clearMarkers();
            }
        }


        if (!show || !poles.length) return;
        const list: any = [];
        for (let i = 0, _list: Array<any> = this.filterItems(poles, search); i < _list.length; i++) {
            const el: any = _list[i];
            const point = el.points.toGPS();
            const image = '/assets/img/Pole.png';
            const poleCircle = new google.maps.Marker({
                position: point,
                clickable: true,
                editable: false,
                draggable: false,
                icon: image
            });
            this.drawedPoles.push(poleCircle);
            this.applyEvent(poleCircle, el);
            list.push(createMarker(poleCircle));


        }
        this.addCluster(3, list);
    }


    private resetDrawMapStations(station: Array<Station>, show: boolean, search: string) {
        this.setMapOnAll(null, this.drawedStations);
        this.drawedStations = [];
        for (let i = 0; i < this.drawedClusters.length; i++) {
            if (this.drawedClusters[i].__type === 2) {
                this.drawedClusters.splice(i--, 1)[0].clearMarkers();
            }
        }
        if (!show || !station.length) return;

        const stationList: any = [];
        for (let i = 0, list: Array<any> = this.filterItems(station, search); i < list.length; i++) {
            const el: any = list[i];
            const point = el.points.toGPS();
            const image = '/assets/img/station.png';
            const stationCircle = new google.maps.Marker({
                position: point,
                clickable: true,
                editable: false,
                draggable: false,
                icon: image
            });

            this.applyEvent(stationCircle, el);
            this.drawedStations.push(stationCircle);
            stationList.push(createMarker(stationCircle));
        }
        this.addCluster(2, stationList);
    }

    private resetDrawMapParcels(_list: Array<Parcel>, show: boolean, search: string) {
        this.setMapOnAll(null, this.drawedParcels);
        this.drawedParcels = [];

        for (let i = 0; i < this.drawedClusters.length; i++) {
            if (this.drawedClusters[i].__type === 1) {
                this.drawedClusters.splice(i--, 1)[0].clearMarkers();
            }
        }
        if (!show || !_list.length) return;
        const points: any = [];
        const polyList: any = [];
        for (let i = 0, list: Array<any> = this.filterItems(_list, search); i < list.length; i++) {
            const el: any = list[i];


            for (let j = 0, gpsList: any = el.points.coordinates; j < gpsList.length; j++) {
                const _el = gpsList[j];
                const path: any = [];
                for (let k = 0; k < _el.length; k++) {
                    for (let kd = 0; kd < _el[k].length; kd++) {
                        path.push(new GPSCoordinate(_el[k][kd]));
                        points.push(path[path.length - 1]);
                    }
                }
                const polygon = createPolygon(path);
                this.drawedParcels.push(polygon);
                this.applyEvent(polygon, el);
                polyList.push(polygon);
            }
        }
        this.addCluster(1, polyList);
        this.fitToView(points);
    }

    private filterItems(list: Array<any>, search: string) {
        let _list = [];
        for (let i = 0; i < list.length; i++) {
            const el: any = list[i];
            if (search) {
                const keys = Object.keys(el);
                let isInseach = false;
                for (let j = 0; j < keys.length; j++) {
                    const val = el[keys[j]];
                    if (val && val.toString().toLowerCase().match(search.toLowerCase())) {
                        isInseach = true;
                        break;
                    }
                }
                if (!isInseach) continue;
            }
            _list.push(el);
        }
        return _list;
    }

    private applyEvent(el: any, item: any) {
        const {drawMode, drawModeList, showDialogContent} = this.props
        google.maps.event.addListener(el, 'click', function () {
            if (item instanceof Parcel) {
                showDialogContent(
                    <AddParcelDialog
                        selectedItem={item}
                    />
                );
            } else if (item instanceof Pole) {
                showDialogContent(
                    <AddPoleDialog
                        selectedItem={item}
                    />
                );
            } else if (item instanceof Segment) {
                showDialogContent(
                    <AddSegmentDialog
                        selectedItem={item}
                    />
                );
            } else if (item instanceof Station) {
                showDialogContent(
                    <AddStationDialog
                        selectedItem={item}
                    />
                );
            } else if (item instanceof Poi) {
                showDialogContent(
                    <AddPoiDialog
                        selectedItem={item}
                        position={new Geometry()}/>
                );
            }
        });
    }

    private addCluster(__type: number, list: Array<any>) {
        const clusterer = new MarkerClusterer(
            this.googleMap,
            list,
            {
                imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
            }
        );
        clusterer.__type = __type;
        this.drawedClusters.push(clusterer);
    }

    private fitToView(points: Array<any>) {
        if (this.googleMap.lastFocusedProjectId === this.props.location.id) return;
        this.googleMap.lastFocusedProjectId = this.props.location.id;
        const bounds = new google.maps.LatLngBounds();


        // Extend bounds with each point
        for (var i = 0; i < points.length; i++) {
            bounds.extend(points[i]);
            // new google.maps.Marker({position: points[i], map: map});
        }

        // Apply fitBounds
        this.googleMap.fitBounds(bounds);

        /*// Draw the bounds rectangle on the map
        const ne = bounds.getNorthEast();
        const sw = bounds.getSouthWest();

        if (this.boundingBox) this.boundingBox.setMap(null);
        const boundingBox = this.boundingBox = new google.maps.Polyline({
            path: [
                ne, new google.maps.LatLng(ne.lat(), sw.lng()),
                sw, new google.maps.LatLng(sw.lat(), ne.lng()), ne
            ],
            strokeColor: '#FF0000',
            strokeOpacity: 0.5,
            strokeWeight: 1
        });

        boundingBox.setMap(this.googleMap);*/
    }

    private drawMap() {
        console.log(this.props.mapZoom);
        const map = this.googleMap = new google.maps.Map(this.container, {
            zoom: this.props.mapZoom,
            center: this.props.mapCenter
        });

        google.maps.event.addListener(map, 'center_changed', (event: any) => {

            this.props.changeContols({
                name: 'mapCenter',
                value: {
                    lat: map.getCenter().lat(),
                    lng: map.getCenter().lng(),
                }
            });
        })
        google.maps.event.addListener(map, 'click', (event: any) => {
            const {location, showAlert, showDialogContent} = this.props;

            if (!location) {
                return showAlert('Please select Project first');
            }
            if (this.props.allowAddPoi) {
                this.drawInMap(event);
            }


        });
        google.maps.event.addListener(map, 'zoom_changed', async () => {
            const project = this.props.location;
            this.props.changeContols({
                name: 'mapZoom',
                value: map.getZoom()
            });
            if (!project) return;
            // await this.props.fetchLocationPoles(project);
            // await this.props.fetchLocationSegments(project);
            // await this.props.fetchLocationStations(project);
            // await this.props.fetchLocationPoi(project);
            // await this.props.fetchLocationParcels(project);
        });
    }

    private drawInMap(event: any) {
        const {drawMode, drawModeList, showDialogContent} = this.props;

        const coordinate = [
            event.latLng.lng(),
            event.latLng.lat()
        ];
        // console.log(coordinate);
        // this.props.changeContols({
        //     name: 'tempPosition',
        //     value: [...this.props.tempPosition, coordinate]
        // });
        showDialogContent(
            <AddPoiDialog
                selectedItem={new Poi()}
                position={new Geometry(Geometry.TYPE.POINT, coordinate)}/>
        );


    }


    render() {
        return (
            <div>
                <div className={'map-store'} ref={(e) => this.container = e}></div>

            </div>
        );
    }
}

const mapStateToProps = (state: any) => ({
    search: searchSelector(state),
    mapCenter: state[moduleName].mapCenter,
    mapZoom: state[moduleName].mapZoom,
    dateFilter: state[moduleName].dateFilter,
    allowAddPoi: state[moduleName].allowAddPoi,
    poiList: state[moduleName].poiList,
    segmentList: state[moduleName].segmentList,
    stationList: state[moduleName].stationList,
    polesList: state[moduleName].polesList,
    parcelList: state[moduleName].parcelList,
    showStations: state[moduleName].showStations,
    showSegments: state[moduleName].showSegments,
    showParcels: state[moduleName].showParcels,
    showPoles: state[moduleName].showPoles,
    showPois: state[moduleName].showPois,
    pois: locationPoisSelector(state),
    tempPosition: lastGeoPostionsSelector(state),
    location: locationSelector(state),
    segments: locationSegmentsSelector(state),
    poles: locationPolesSelector(state),
    parcels: locationParcelsSelector(state),
    stations: locationStationsSelector(state),
    drawMode: currentModeSelector(state),
    drawModeList: modesSelector(state),
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        changeContols,
        showDialogContent,
        fetchLocationPoi,
        fetchLocationParcels,
        fetchLocationPoles,
        fetchLocationSegments,
        fetchLocationStations,
        showAlert
    }, dispatch)
);
const map = connect(mapStateToProps, mapDispatchToProps)(MapContainer);
export default map;






