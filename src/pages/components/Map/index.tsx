import React, {Component} from 'react';
import './index.scss';
import MapContainer from './map.container';
import {Col, Row} from "antd";

import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import {Parcel, Pole, Segment, Station} from "../../../entities";
import {bindActionCreators} from "redux";
import {
    changeContols, currentModeSelector,
    lastGeoPostionsSelector, locationParcelsSelector,
    locationPoisSelector, locationPolesSelector, locationSegmentsSelector,
    locationSelector, locationStationsSelector, modesSelector,
    moduleName
} from "../../../ducks/map";
import {showAlert, showDialogContent} from "../../../ducks/dialogs";
import {fetchLocationPoi} from "../../../ducks/map/poi";
import {fetchLocationParcels} from "../../../ducks/map/parcels";
import {fetchLocationPoles} from "../../../ducks/map/poles";
import {fetchLocationSegments} from "../../../ducks/map/segments";
import {fetchLocationStations} from "../../../ducks/map/stations";
import {connect} from "react-redux";


interface MapContainerProps {


    changeContols: any
    allowAddPoi: any
}

class MapSection extends Component<MapContainerProps,
    {}> {


    componentDidMount(): void {
    }

    private onAllowToaddPoi = () => {
        this.props.changeContols({
            name: 'allowAddPoi',
            value: Date.now()
        });
        this.props.changeContols({
            name: 'showPois',
            value: true
        });
    }


    render() {
        return (
            <div className={'fullWidth'}>
                <Row>
                    {/*<Col span={12}>*/}
                    {/*<MapControls/>*/}
                    {/*</Col>*/}
                    {
                        this.props.allowAddPoi && (
                            <div className={'help-area add-poi'}>
                                Click on the map to set the location
                            </div>
                        )
                    }
                    <Col span={12}>
                        <MapContainer/>
                        <div className={'fab-icon-add'}>
                            <Fab color="primary" aria-label="Add" onClick={this.onAllowToaddPoi}>
                                <AddIcon/>
                            </Fab>
                        </div>
                    </Col>
                    <Col span={12}>

                    </Col>
                </Row>
            </div>
        );
    }
}


const mapStateToProps = (state: any) => ({
    allowAddPoi: state[moduleName].allowAddPoi,
});
const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        changeContols,
    }, dispatch)
);
const map = connect(mapStateToProps, mapDispatchToProps)(MapSection);
export default map;
