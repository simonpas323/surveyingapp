import React, {Component} from 'react';
import './index.scss';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Button from '@material-ui/core/Button';
import {showAlert, alertTextSelector} from "../../../../ducks/dialogs";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";


class AlertDialog extends Component<{ showAlert: any, alertText: string },
    {}> {


    private handleClose = () => {
        this.props.showAlert(false);
    };

    render() {
        return (
            <Dialog
                open={!!this.props.alertText}
                onClose={this.handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Warning"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {this.props.alertText}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>

                    <Button onClick={this.handleClose} color="primary" autoFocus>
                        Close
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}

const mapStateToProps = (state: any) => ({
    alertText: alertTextSelector(state)
});
const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showAlert,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(AlertDialog);

