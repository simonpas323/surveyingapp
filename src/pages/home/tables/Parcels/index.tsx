import React, {Component} from 'react';
import './index.scss';

import {onLoadMoreItems, deleteParcel} from "../../../../ducks/map/parcels";
import {locationPoisSelector, locationSelector, moduleName} from "../../../../ducks/map";
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import MainTable from "../MainTable";
import {showDialogContent} from "../../../../ducks/dialogs";
import {API} from "../../../../config";
import Title from "../../../../components/title";
import AddParcelDialog from "../../../components/Map/map.container/add.parcel";

class ParcelsTable extends MainTable {

    constructor(p: any) {
        super(p);
        const columns: any = [
            {name: 'id', title: 'Id', getCellValue: (row: any) => <Title>{row.id}</Title>},
            {name: 'title', title: 'Title', getCellValue: (row: any) => <Title key={row.title}>{row.title}</Title>},
            {
                name: 'wojewodztw',
                title: 'Wojewodztw',
                getCellValue: (row: any) => <Title key={row.wojewodztw}>{row.wojewodztw}</Title>
            },
            {name: 'gmina', title: 'Gmina', getCellValue: (row: any) => <Title key={row.gmina}>{row.gmina}</Title>},
        ];
        const tableColumnExtensions: any = [
            {columnName: 'id', width: 200},
            {columnName: 'title', width: 200},
            {columnName: 'wojewodztw', width: 220},
            {columnName: 'gmina', width: 220}
        ];
        this.state = {
            ...this.state,
            tableColumnExtensions,
            columns,
            title: 'Parcels'
        };
    }

    componentDidMount(): void {
        super.componentDidMount();

    }


    protected URL = (): string => {
        return `${API}api/projects/${this.props.project ? this.props.project.id : -1}/parcels`;
    };


    protected onEditItem = (item: any, onFinishEditItem: Function = () => false) => {
        const {showDialogContent} = this.props;
        showDialogContent(
            <AddParcelDialog
                selectedItem={item}
                onFinishEditItem={() => this.callbacks.push(onFinishEditItem)}
            />
        );
    };


    render() {
        return super._render();
    }
}

const mapStateToProps = (state: any) => ({
    itemsList: state[moduleName].parcelList,
    project: locationSelector(state),
    rows: locationPoisSelector(state)
});
const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
        onLoadMoreItems: onLoadMoreItems,
        onDeleteItem: deleteParcel
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(ParcelsTable);
