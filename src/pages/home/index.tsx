import React, {Component} from 'react';
import './index.scss';

import {signOut, userSelector} from '../../ducks/auth';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {Redirect, Route, Switch} from "react-router";
import HomeTablesPage from "./tables";
import HomeMapPage from "./map";


class HomePage extends Component<{ signOut: any, user: any },
    {}> {


    render() {
        return (
            <Switch>
                <Route path='/home/map' component={HomeMapPage}/>
                <Route path='/home/tables' component={HomeTablesPage}/>
                <Route exact path="*" render={() => (
                    <Redirect to="/home/map"/>
                )}/>
            </Switch>
        );
    }
}

const mapStateToProps = (state: any) => ({
    user: userSelector(state)
});
const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        signOut,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
