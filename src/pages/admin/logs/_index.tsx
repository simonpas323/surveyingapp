import React, {Component} from 'react';
import './index.scss';

import {USER_ROLE} from '../../../ducks/auth';
import {errorSelector, logSelector} from '../../../ducks/admin';
import {fetchLOGS} from '../../../ducks/admin/logs';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {showDialogContent} from "../../../ducks/dialogs"

import DeleteItemDialog from '../../../components/DeleteItemDialog';
import ReactVirtualizedTable from '../../../components/table';
import {Log} from "../../../entities";


interface LogsPageProps {
    logs: Array<Log>,
    showDialogContent: Function,
    fetchLOGS: Function,
}

class LogsPage extends Component<LogsPageProps,
    {}> {


    componentDidMount(): void {
        this.props.fetchLOGS();
    }

    private onDeleteItem = (item: Log) => {
    };
    private onEditItem = (item: Log) => {

    };
    private onRowClick = () => {

    };
    private onSelectAllClick = () => {

    };
    private addUser = () => {

    };
    private handleChange = (item: Log, ev: any) => {
    }

    render() {
        const columns = [
            {
                minWidth: 120,
                label: 'IP',
                dataKey: 'ip',
            },
            {
                minWidth: 120,
                label: 'Action',
                dataKey: 'action',
                render: (item: any) => {
                    return (<span>{item.user ? item.user.email : ''} {item.action}</span>)
                }
            },
            {
                minWidth: 120,
                label: 'Description',
                dataKey: 'description',
                render: (item: any) => {
                    return (<span>{item.description}</span>)
                }
            },
            {
                minWidth: 120,
                label: 'Date Created',
                dataKey: 'createdAt'
            }
        ];

        return (
            <div className={'d-flex f-col'}>
                <ReactVirtualizedTable
                    cellheight={84}
                    hasActions={false}
                    height={500}
                    width={500}
                    hasSelection={false}
                    labelKey={''}
                    selected={[]}
                    searchText={''}
                    onDeleteItem={this.onDeleteItem}
                    onEditItem={this.onEditItem}
                    onRowClick={this.onRowClick}
                    onSelectAllClick={this.onSelectAllClick}
                    items={this.props.logs}
                    columns={columns}
                    rows={this.props.logs}
                />
            </div>

        );

    }
}

const mapStateToProps = (state: any) => ({

    error: errorSelector(state),
    logs: logSelector(state)
});
const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
        fetchLOGS,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(LogsPage);
