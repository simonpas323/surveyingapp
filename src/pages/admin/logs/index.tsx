import React, {Component} from 'react';
import './index.scss';

import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {showDialogContent} from "../../../ducks/dialogs";
import {fetchMoreLOGS} from "../../../ducks/admin/logs";
import {API} from "../../../config";
import Title from "../../../components/title";
import {moduleName} from "../../../ducks/admin/config";
import MainTable from "../../home/tables/MainTable";
import {locationPoisSelector, locationSelector} from "../../../ducks/map";
import {errorSelector} from "../../../ducks/admin";

class LogsTable extends MainTable {

    constructor(p: any) {
        super(p);
        const columns: any = [
            {name: 'IP', title: 'ip', getCellValue: (row: any) => <Title>{row.ip}</Title>},
            {
                name: 'Action',
                title: 'action',
                getCellValue: (item: any) => (
                    <Title>{item.user ? item.user.email : ''} {item.action}</Title>)
            },
            {name: 'Description', title: 'description', getCellValue: (row: any) => <Title>{row.description}</Title>},
        ];
        const tableColumnExtensions: any = [
            {columnName: 'IP', width: 200},
            {columnName: 'Action', width: 400, filteringEnabled: false, sortingEnabled: false},
            {columnName: 'Description', width: 220},
        ];
        const title: any = 'Logs';
        this.state = {
            ...this.state,
            HAVE_NO_PROJECT: true,
            hasDeleteAction: false,
            hasEditAction: false,
            tableColumnExtensions,
            columns,
            title
        };
    }


    protected URL = (): string => {
        return `${API}api/admin/logs`;
    };


    render() {
        return super._render();
    }
}

const mapStateToProps = (state: any) => ({
    itemsList: state[moduleName].userList,
    project: locationSelector(state),
    error: errorSelector(state),
    rows: locationPoisSelector(state)
});
const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
        onLoadMoreItems: fetchMoreLOGS,
        onDeleteItem: () => 0
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(LogsTable);
