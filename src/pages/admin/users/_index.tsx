import React, {Component} from 'react';
import './index.scss';

import {USER_ROLE} from '../../../ducks/auth';
import {errorSelector, usersSelector} from '../../../ducks/admin';
import {addUser, editUser, fetchUsers, deleteUser} from '../../../ducks/admin/users';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {showDialogContent} from "../../../ducks/dialogs"

import DeleteItemDialog from '../../../components/DeleteItemDialog';
import ReactVirtualizedTable from '../../../components/table';
import AddEditUserDialog from "./add.edit.users";
import {Geometry, Poi, User} from "../../../entities";
import {Button} from "@material-ui/core";
import {checkError} from "../../utils";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import {CSVLink} from "react-csv";


interface UserPageProps {
    users: Array<User>,
    showDialogContent: Function,
    fetchUsers: Function,
    editUser: Function,
    addUser: Function,
    deleteUser: Function,
}

class UsersPage extends Component<UserPageProps,
    {}> {

    componentWillReceiveProps(nextProps: Readonly<UserPageProps>, nextContext: any): void {
        checkError(nextProps, this.props, () => {
        });
    }

    private onDeleteItem = (item: User) => {
        const {showDialogContent} = this.props;
        showDialogContent(
            <DeleteItemDialog
                title={`User(${item.email})`}
                onCancel={() => showDialogContent(null)}
                onAccept={() => {
                    this.props.deleteUser(item);
                    showDialogContent(null);
                }}
            />
        );
    };
    private onEditItem = (item: User) => {
        const {showDialogContent} = this.props;
        showDialogContent(
            <AddEditUserDialog
                selectedItem={item}
            />
        );
    };
    private onRowClick = () => {

    };
    private onSelectAllClick = () => {

    };
    private addUser = () => {
        const {showDialogContent} = this.props;
        showDialogContent(
            <AddEditUserDialog
                selectedItem={new User()}
            />
        );
    };
    private handleChange = (item: User, ev: any) => {
        this.props.editUser({...item, role: parseInt(ev.target.value)})
    }

    render() {
        const columns = [
            {
                minWidth: 120,
                label: 'Email',
                dataKey: 'email',
            },
            {
                minWidth: 120,
                label: 'FullName',
                dataKey: 'firstName',
                render: (item: any) => {
                    return (<span>{item.firstName} {item.secondName}</span>)
                }
            },
            {
                minWidth: 120,
                label: 'User Role',
                dataKey: 'role',
                render: (item: any) => {
                    return (
                        <FormControl>
                            <RadioGroup
                                className={'d-flex f-row radio-group'}
                                aria-label="Gender"
                                value={item.role}
                                onChange={(role: any) => this.handleChange(item, role)}
                            >
                                <FormControlLabel value={USER_ROLE.SUPER_USER} control={<Radio/>} label="Super Admin"/>
                                <FormControlLabel value={USER_ROLE.ADMIN} control={<Radio/>} label="Admin"/>
                                <FormControlLabel value={USER_ROLE.USER} control={<Radio/>} label="User"/>
                            </RadioGroup>
                        </FormControl>
                    )
                }
            },
            {
                minWidth: 120,
                label: 'Date Created',
                dataKey: 'createdAt'
            }
        ];


        return (
            <div className={'d-flex f-col'}>
                <div>
                    <Button variant="contained" onClick={this.addUser} className={'my-btn btn-primary'}>
                        Add
                    </Button>
                </div>
                <ReactVirtualizedTable
                    cellheight={84}
                    hasActions={true}
                    height={500}
                    width={500}
                    hasSelection={false}
                    labelKey={''}
                    selected={[]}
                    searchText={''}
                    onDeleteItem={this.onDeleteItem}
                    onEditItem={this.onEditItem}
                    onRowClick={this.onRowClick}
                    onSelectAllClick={this.onSelectAllClick}
                    items={this.props.users}
                    columns={columns}
                    rows={this.props.users}
                />
            </div>

        );

    }
}

const mapStateToProps = (state: any) => ({

    error: errorSelector(state),
    users: usersSelector(state)
});
const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
        fetchUsers,
        editUser,
        addUser,
        deleteUser
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(UsersPage);
