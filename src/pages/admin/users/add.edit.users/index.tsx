import React, {Component} from 'react';
import './index.scss';
import {addUser, editUser} from "../../../../ducks/admin/users";
import {errorSelector, moduleName} from "../../../../ducks/admin";
import {showDialogContent} from "../../../../ducks/dialogs";
import {bindActionCreators} from "redux";
import {connect, Provider} from "react-redux";

import {checkError} from '../../../utils';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {ValidatorForm, TextValidator} from 'react-material-ui-form-validator';

import {
    User
} from "../../../../entities";

const ValidF: any = ValidatorForm;


interface UserPageProps {
    userItemEdited: any,
    userItemAdded: any,
    error: any,
    selectedItem: User,
    addItem: Function,
    onFinishEditItem: Function,
    editItem: Function,
    showDialogContent: Function
}

class AddEditUserDialog extends Component<UserPageProps,
    {
        password: string,
        pending: boolean,
        repeatPassword: string,
        id: any,
        disabled: boolean
    }> {

    constructor(p: any) {
        super(p);
        this.state = {
            email: '',
            firstName: '',
            secondName: '',
            repeatPassword: '',
            id: '',
            ...p.selectedItem,
            password: '',
            pending: false,
            disabled: false
        };
    }

    static defaultProps = {
        onFinishEditItem: () => 1
    }

    componentDidMount(): void {
        ValidF.addValidationRule('isPasswordMatch', (value: string) => {
            if (value !== this.state.password) {
                return false;
            }
            return true;
        });
    }

    componentWillUnmount() {
        ValidF.removeValidationRule('isPasswordMatch');
    }

    componentWillReceiveProps(nextProps: Readonly<UserPageProps>, nextContext: any): void {
        checkError(nextProps, this.props, () => {
            this.setState({
                pending: false,
                disabled: false,
            });
        });
        if (nextProps.userItemAdded !== this.props.userItemAdded || nextProps.userItemEdited !== this.props.userItemEdited) {
            this.handleCancel();
            this.setState({
                pending: false,
                disabled: false,
            });
        }
    }

    private onChange = (e: any) => {
        const newState: any = {
            disabled: false,
            [e.target.name]: e.target.value
        };
        this.setState(newState);
    };


    private handleOk = async (e: any) => {
        const form: any = this.refs.form;
        form.submit();
    };

    private handleCancel = () => {
        this.props.showDialogContent(null);
    };

    private handleSubmit = (e: any) => {
        const form: any = this.refs.form;
        form.isFormValid().then(async (valid: any) => {
            if (valid) {
                this.setState({
                    pending: true,
                });
                if (this.state.id) {
                    await this.props.editItem({
                        ...this.state,
                    });
                } else {
                    await this.props.addItem({
                        ...this.state,
                    });
                }

                this.props.onFinishEditItem();
            } else {
                this.setState({
                    disabled: true,
                });
            }
        })
    }

    render() {
        const {email, firstName, secondName, password, repeatPassword, id, disabled, pending}: any = this.state;

        return (
            <React.Fragment>

                <DialogTitle id="max-width-dialog-title">{id ? 'Edit' : 'Add'} user</DialogTitle>
                <DialogContent>

                    <ValidatorForm
                        ref="form"
                        onSubmit={this.handleSubmit}
                        onError={(errors: any) => console.log(errors)}
                        className={'d-flex f-col'}
                    >
                        <div className={'d-flex'}>
                            <div className={'m-5'}>
                                <TextValidator
                                    className={'m-5'}
                                    label="FirstName"
                                    onChange={this.onChange}
                                    name="firstName"
                                    autoComplete={'off'}
                                    value={firstName}
                                />
                            </div>
                            <div className={'m-5'}>
                                <TextValidator
                                    className={'m-5'}
                                    label="secondName"
                                    onChange={this.onChange}
                                    name="secondName"
                                    autoComplete={'off'}
                                    value={secondName}
                                />
                            </div>
                        </div>
                        <div className={'m-5'}>
                            <TextValidator
                                className={'m-5'}
                                label="Email"
                                onChange={this.onChange}
                                name="email"
                                autoComplete={'off'}
                                value={email}
                                validators={['required', 'isEmail']}
                                errorMessages={['this field is required', 'email is not valid']}
                            />
                        </div>
                        {/*<div className={'d-flex'}>
                            <div className={'m-5'}>
                                <TextValidator
                                    className={'m-5'}
                                    autoComplete={'off'}
                                    label="Password"
                                    onChange={this.onChange}
                                    name="password"
                                    type="password"
                                    validators={!id ? ['required'] : []}
                                    errorMessages={!id ? ['this field is required'] : []}
                                    value={password}
                                />
                            </div>
                            <div className={'m-5'}>
                                <TextValidator
                                    label="Repeat password"
                                    autoComplete={'off'}
                                    onChange={this.onChange}
                                    name="repeatPassword"
                                    type="password"
                                    validators={!id ? ['isPasswordMatch', 'required'] : ['isPasswordMatch']}
                                    errorMessages={!id || this.state.password ? ['password mismatch', 'this field is required'] : ['password mismatch']}
                                    value={repeatPassword}
                                />
                            </div>
                        </div>*/}

                    </ValidatorForm>

                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleCancel} color="primary">
                        Close
                    </Button>
                    <Button onClick={this.handleOk} disabled={disabled || pending}>
                        Save
                    </Button>
                </DialogActions>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state: any) => ({
    userItemEdited: state[moduleName].userItemEdited,
    userItemAdded: state[moduleName].userItemAdded,
    error: errorSelector(state),
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        showDialogContent,
        addItem: addUser,
        editItem: editUser,
    }, dispatch)
);
const edit = connect(mapStateToProps, mapDispatchToProps)(AddEditUserDialog);
export default edit;
