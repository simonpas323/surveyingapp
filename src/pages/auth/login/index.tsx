import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { signIn, moduleName } from '../../../ducks/auth';
import AVA from '../../../assets/img/Group 2.png';
import './login.scss';
import Loading from "../../../components/loading";

class Login extends Component<{ signIn: any, authError: boolean , loading: boolean },
  {}> {

  state = {
    error: false,
    email: '',
    password: '',
  };


  onInput = (e: any) => {
    this.setState({
      [e.target.name]: e.target.value,
      error: false,
    });
  };
  onSubmit = async (e: any) => {
    e.preventDefault();
    e.stopPropagation();
    const { email, password } = this.state;
    if (email && password) {
      try {
        await this.props.signIn(this.state);
      } catch (e) {
        this.setState({
          error: 'Error! Either user or password are wrong. Please try again',
        });
      }

    }
  };

  render() {
    const { authError } = this.props;
    return (
      <div className={'login-page d-flex'}>
        <div className={'col-6  '}>
          <div className={'d-flex a-c fullHeight '}>
            <div className={'d-flex a-c f-col form-container'}>
              <div className={'login-title fullWidth'}>Welcome  </div>

              <form className={'login-form-body fullWidth'}>
                <div className={'form-item d-flex f-col '}>
                  <label>Email</label>
                  <input name={'email'} id={'userName'} value={this.state.email} onChange={this.onInput}
                         required/>
                </div>
                <div className={'form-item d-flex f-col'}>
                  <label>Password</label>
                  <input name={'password'} id={'userPassword'} value={this.state.password}
                         onChange={this.onInput}
                         type={'password'}
                         required/>
                </div>


                <br/>
                <div className={'form-actions d-flex a-c j-start'}>
                  <button type={'submit'} className={'my-btn my-success'} disabled={this.props.loading} onClick={this.onSubmit}>
                    Sign in
                    {this.props.loading && <Loading/>}
                  </button>

                  <Link to="/forgot-psw">Forgot Password</Link>
                  &nbsp;
                  &nbsp;
                  &nbsp;
                  {/*<Link to="/register">Sign UP</Link>*/}

                </div>
              </form>
              {
                authError && (<p style={{ color: 'red' }} className={'error-message'}>
                  Error! Either user or password are wrong. Please try again
                </p>)
              }
              {/*<div className={'fullWidth new-here'}>New to Patient 360? <a href={'#'}>Create an account</a>.</div>*/}

            </div>

          </div>
          <div className={'fullWidth terms'}>
            Terms of Use &nbsp;&nbsp;|&nbsp;&nbsp;    Privacy Policy &nbsp;&nbsp;   |   &nbsp;&nbsp; Contact Us
          </div>
        </div>
        <div className={'col-6'}>
          <div className={'d-flex a-c fullHeight fullWidth'}>
            <div className={'d-flex a-c fullHeight j-center help-area fullWidth'}>

              {/*<img src={AVA} style={{ height: '80vh' }}/>*/}
            </div>
          </div>
        </div>


      </div>

    );
  }
}

const mapStateToProps = (state: any) => ({
  authError: state[moduleName].error,
  loading: state[moduleName].loading,
});

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    signIn,
  }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(Login);
