import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {resetPsw, moduleName} from '../../../ducks/auth';
import './login.scss';
import {Link, RouteComponentProps, withRouter} from "react-router-dom";
import Loading from "../../../components/loading";

interface MatchParams {
    MemberID: string;
}

interface Props extends RouteComponentProps<MatchParams> {
    refreshed: any,
    resetPsw: any,
    loading: boolean,
    authError: boolean
}

class ResetPsw extends Component<Props,
    {}> {

    state = {
        pending: false,
        error: false,
        reppassword: '',
        password: '',
    };

    componentWillReceiveProps(nextProps: Readonly<Props>, nextContext: any): void {
        if (nextProps.refreshed !== this.props.refreshed) {
            this.props.history.push("/login");
        }
    }
    onInput = (e: any) => {
        this.setState({
            [e.target.name]: e.target.value,
            error: false,
        });
    };
    onSubmit = async (e: any) => {
        e.preventDefault();
        e.stopPropagation();
        const {reppassword, password} = this.state;
        if (!password) {
            return this.setState({
                error: 'Password is required'
            })
        }
        if (password !== reppassword) {
            return this.setState({
                error: 'Passwords doesn`t match'
            })
        }
        try {
            await this.props.resetPsw({
                ...this.state,
                tempForgotPswLink: location.href.split("?")[1].split("=")[1]

            });
        } catch (e) {
            this.setState({
                error: e.message || e
            });
        }

    };

    render() {
        const {authError} = this.props;
        const {error} = this.state;
        return (
            <div className={'login-page d-flex'}>
                <div className={'col-6  '}>
                    <div className={'d-flex a-c fullHeight '}>
                        <div className={'d-flex a-c f-col form-container'}>
                            <div className={'login-title fullWidth'}>Welcome</div>

                            <form className={'login-form-body fullWidth'}>
                                <div className={'form-item d-flex f-col'}>
                                    <label>Password</label>
                                    <input name={'password'} id={'userPassword'} value={this.state.password}
                                           onChange={this.onInput}
                                           type={'password'}
                                           required/>
                                </div>
                                <div className={'form-item d-flex f-col'}>
                                    <label>Repeat Password</label>
                                    <input name={'reppassword'} id={'userPassword'} value={this.state.reppassword}
                                           onChange={this.onInput}
                                           type={'password'}
                                           required/>
                                </div>


                                <br/>
                                <div className={'form-actions d-flex a-c j-start'}>
                                    <button type={'submit'}
                                            disabled={this.props.loading}
                                            className={'my-btn my-success'}
                                            onClick={this.onSubmit}>
                                        Reset password
                                        {this.props.loading && <Loading/>}
                                    </button>

                                    <Link to="/">Back to Login</Link>
                                </div>
                            </form>
                            {
                                authError && (<p style={{color: 'red'}} className={'error-message'}>
                                    {authError}
                                </p>)
                            }
                            {
                                error && (<p style={{color: 'red'}} className={'error-message'}>
                                    {error}
                                </p>)
                            }

                        </div>

                    </div>
                    <div className={'fullWidth terms'}>
                        Terms of Use &nbsp;&nbsp;|&nbsp;&nbsp;    Privacy Policy &nbsp;&nbsp;   |   &nbsp;&nbsp; Contact
                        Us
                    </div>
                </div>
                <div className={'col-6'}>
                    <div className={'d-flex a-c fullHeight fullWidth'}>
                        <div className={'d-flex a-c fullHeight j-center help-area fullWidth'}>

                            {/*<img src={AVA} style={{ height: '80vh' }}/>*/}
                        </div>
                    </div>
                </div>


            </div>

        );
    }
}

const mapStateToProps = (state: any) => ({
    refreshed: state[moduleName].refreshed,
    authError: state[moduleName].error,
    loading: state[moduleName].loading,
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        resetPsw,
    }, dispatch)
);
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ResetPsw));
