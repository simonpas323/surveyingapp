import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {reqResetPsw, moduleName} from '../../../ducks/auth';
import './login.scss';
import {Link, RouteComponentProps, withRouter} from "react-router-dom";
import Loading from "../../../components/loading";

interface MatchParams {
    MemberID: string;
}

interface Props extends RouteComponentProps<MatchParams> {
    reqResetPsw: any,
    authError: boolean,
    loading: boolean,
    refreshed: number
}

class ForgotPsw extends Component<Props,
    {}> {

    state = {
        error: false,
        email: '',
    };


    componentWillReceiveProps(nextProps: Readonly<Props>, nextContext: any): void {
        if (nextProps.refreshed !== this.props.refreshed) {
            this.props.history.push("/login");
        }
    }

    onInput = (e: any) => {
        this.setState({
            [e.target.name]: e.target.value,
            error: false,
        });
    };
    onSubmit = async (e: any) => {
        e.preventDefault();
        e.stopPropagation();
        const {email} = this.state;
        if (email) {
            await this.props.reqResetPsw(this.state);
        }
    };

    render() {
        const {authError} = this.props;
        return (
            <div className={'login-page d-flex'}>
                <div className={'col-6  '}>
                    <div className={'d-flex a-c fullHeight '}>
                        <div className={'d-flex a-c f-col form-container'}>
                            <div className={'login-title fullWidth'}>Welcome</div>

                            <form className={'login-form-body fullWidth'}>
                                <div className={'form-item d-flex f-col '}>
                                    <label>Email</label>
                                    <input type="email" name={'email'} id={'userName'} value={this.state.email}
                                           onChange={this.onInput}
                                           required/>
                                </div>


                                <br/>
                                <div className={'form-actions d-flex a-c j-start'}>
                                    <button type={'submit'} className={'my-btn my-success'}
                                            disabled={this.props.loading}
                                            onClick={this.onSubmit}>
                                        Reset password
                                        {this.props.loading && <Loading/>}

                                    </button>
                                    &nbsp;
                                    &nbsp;
                                    &nbsp;
                                    <Link to="/">Back to Login</Link>

                                </div>
                            </form>
                            {
                                authError && (<p style={{color: 'red'}} className={'error-message'}>
                                    Error! User not found
                                </p>)
                            }

                        </div>

                    </div>
                    <div className={'fullWidth terms'}>
                        Terms of Use &nbsp;&nbsp;|&nbsp;&nbsp;    Privacy Policy &nbsp;&nbsp;   |   &nbsp;&nbsp; Contact
                        Us
                    </div>
                </div>
                <div className={'col-6'}>
                    <div className={'d-flex a-c fullHeight fullWidth'}>
                        <div className={'d-flex a-c fullHeight j-center help-area fullWidth'}>

                            {/*<img src={AVA} style={{ height: '80vh' }}/>*/}
                        </div>
                    </div>
                </div>


            </div>

        );
    }
}

const mapStateToProps = (state: any) => ({
    refreshed: state[moduleName].refreshed,
    authError: state[moduleName].error,
    loading: state[moduleName].loading,
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        reqResetPsw,
    }, dispatch)
);
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ForgotPsw));
