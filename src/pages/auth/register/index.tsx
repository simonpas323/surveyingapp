import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import {signUp, moduleName} from '../../../ducks/auth';
import './login.scss';

class Register extends Component<{ signUp: any, authError: boolean },
    {}> {

    state = {
        error: false,
        email: '',
        lastName: '',
        firstName: '',
        reppassword: '',
        password: '',
    };


    onInput = (e: any) => {
        this.setState({
            [e.target.name]: e.target.value,
            error: false,
        });
    };
    onSubmit = async (e: any) => {
        e.preventDefault();
        e.stopPropagation();
        const {email, password, reppassword} = this.state;

        if (!email || !password) {
            return this.setState({
                error: 'email and password are required',
            });
        }
        if (password !== reppassword) {
            return this.setState({
                error: 'Passwords don`t match',
            });
        }

        try {
            this.setState({
                error: null,
            });
            await this.props.signUp(this.state);
        } catch (e) {
            this.setState({
                error: 'Error! can`t register new user',
            });
        }
    };

    render() {
        const {authError} = this.props;
        const {error} = this.state;
        return (
            <div className={'login-page d-flex'}>
                <div className={'col-6  '}>
                    <div className={'d-flex a-c fullHeight '}>
                        <div className={'d-flex a-c f-col form-container'}>
                            <div className={'login-title fullWidth'}>Welcome</div>

                            <form className={'login-form-body fullWidth'}>
                                <div className={'form-item d-flex f-col '}>
                                    <label>FirstName</label>
                                    <input name={'firstName'} id={'firstName'} value={this.state.firstName}
                                           onChange={this.onInput}
                                           required/>
                                </div>
                                <div className={'form-item d-flex f-col '}>
                                    <label>LastName</label>
                                    <input name={'firstName'} id={'firstName'} value={this.state.lastName}
                                           onChange={this.onInput}
                                           required/>
                                </div>
                                <div className={'form-item d-flex f-col '}>
                                    <label>Email</label>
                                    <input name={'email'} id={'userName'} value={this.state.email}
                                           onChange={this.onInput}
                                           required/>
                                </div>
                                <div className={'form-item d-flex f-col'}>
                                    <label>Password</label>
                                    <input name={'password'} id={'userPassword'} value={this.state.password}
                                           onChange={this.onInput}
                                           type={'password'}
                                           required/>
                                </div>
                                <div className={'form-item d-flex f-col'}>
                                    <label>Repeat Password</label>
                                    <input name={'reppassword'} id={'userPassword'} value={this.state.reppassword}
                                           onChange={this.onInput}
                                           type={'password'}
                                           required/>
                                </div>


                                <br/>
                                <div className={'form-actions d-flex a-c j-start'}>
                                    <button type={'submit'} className={'my-btn my-success'} onClick={this.onSubmit}>Sign
                                        UP
                                    </button>

                                    <Link to="/">Back to login</Link>

                                </div>
                            </form>
                            {
                                (authError) && (<p style={{color: 'red'}} className={'error-message'}>
                                    {authError}
                                </p>)
                            }
                            {
                                (error) && (<p style={{color: 'red'}} className={'error-message'}>
                                    {error}
                                </p>)
                            }

                        </div>

                    </div>
                    <div className={'fullWidth terms'}>
                        Terms of Use &nbsp;&nbsp;|&nbsp;&nbsp;    Privacy Policy &nbsp;&nbsp;   |   &nbsp;&nbsp; Contact
                        Us
                    </div>
                </div>
                <div className={'col-6'}>
                    <div className={'d-flex a-c fullHeight fullWidth'}>
                        <div className={'d-flex a-c fullHeight j-center help-area fullWidth'}>

                            {/*<img src={AVA} style={{ height: '80vh' }}/>*/}
                        </div>
                    </div>
                </div>


            </div>

        );
    }
}

const mapStateToProps = (state: any) => ({
    authError: state[moduleName].error,
});

const mapDispatchToProps = (dispatch: any) => (
    bindActionCreators({
        signUp,
    }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(Register);
