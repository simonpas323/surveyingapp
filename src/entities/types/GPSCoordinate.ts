export class GPSCoordinate {
    lat: number;
    lng: number;

    constructor(coordinate: Array<number>) {
        this.lat = coordinate[1];
        this.lng = coordinate[0];
    }
}
