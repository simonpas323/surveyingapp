import Main from './Main';
import {Geometry} from './Geometry';

export class Pole extends Main {
    num_slup: string;

    constructor(data: any) {
        super(data);

        this.num_slup = data.num_slup || '';
        if (data instanceof Pole) return data;
    }
}
