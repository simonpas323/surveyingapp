import Main from './Main';
import {Geometry} from './Geometry';

export class Parcel extends Main {
    wojewodztw: string;
    gmina: string;

    constructor(data: any = {points: new Geometry()}) {

        super(data);
        this.gmina = data.gmina || '';
        this.wojewodztw = data.wojewodztw || '';
        if (data instanceof Parcel) return data;
    }
}
