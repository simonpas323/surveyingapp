import {GPSCoordinate} from './GPSCoordinate';

import _ from 'lodash';

export const Geometry_TYPE: any = {
    POINT: 'Point',
    POLYGON: 'Polygon',
    MultiLineString: 'MultiLineString',
    MultiPolygon: 'MultiPolygon',
}

export class Geometry {
    type: string;
    coordinates: Array<any> = [];

    static TYPE = {
        POINT: Geometry_TYPE.POINT,
        POLYGON: Geometry_TYPE.POLYGON,
        MultiLineString: Geometry_TYPE.MultiLineString,
        MultiPolygon: Geometry_TYPE.MultiPolygon,
    };

    constructor(type: string = Geometry_TYPE.POINT, coordinates: Array<any> = []) {
        this.type = type;

        switch (type) {
            case Geometry.TYPE.MultiPolygon: {
                this.coordinates = coordinates;
                break;
            }
            case Geometry.TYPE.MultiLineString: {
                this.coordinates = coordinates;
                break;
            }
            case Geometry.TYPE.POLYGON: {
                this.coordinates = [coordinates];
                break;
            }
            case Geometry.TYPE.POINT: {
                this.coordinates = coordinates;
                break;
            }
        }

    }

    toGPS(): any {
        const coordinate = JSON.parse(JSON.stringify((this.coordinates)));
        switch (this.type) {
            case Geometry_TYPE.POINT: {
                return new GPSCoordinate(coordinate)
            }
            case Geometry_TYPE.MultiLineString: {
                return coordinate.map((els: Array<Array<number>>) => {
                    // return el.map((els: Array<Array<number>>) => {
                    return els.map((point: any) => new GPSCoordinate(point));
                    // })
                });
            }
            case Geometry_TYPE.POLYGON: {
                return coordinate.map((el: Array<Array<Array<number>>>) => {
                    return el.map((els: Array<Array<number>>) => {
                        return els.map((point: Array<number>) => new GPSCoordinate(point));
                    })
                });
            }
            case Geometry_TYPE.MultiPolygon: {
                return coordinate.map((el: Array<Array<Array<number>>>) => {
                    return el.map((els: Array<Array<number>>) => {
                        return els.map((point: Array<number>) => new GPSCoordinate(point));
                    })
                });
            }
        }
    }
}
