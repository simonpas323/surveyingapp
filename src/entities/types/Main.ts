import {Geometry} from "./Geometry";

export default class Main {
    id: number;
    createdAt: any;
    updatedAt: any;
    deletedAt: any;
    status: any = 1;
    type: string = '';
    title: string = '';
    description: string = '';
    projectId: number = -1;
    userId: number = -1;
    points: Geometry = new Geometry();

    constructor(data: any = {points: new Geometry()}) {

        this.id = data.id;
        this.createdAt = data.createdAt;
        this.updatedAt = data.updatedAt;
        this.deletedAt = data.deletedAt;
        this.status = data.status;
        this.title = data.title;
        this.description = data.description;
        this.userId = data.userId;
        this.projectId = data.projectId;
        if (data.points) {
            const points = typeof data.points === 'string' ? JSON.parse(data.points) : data.points;
            this.points = new Geometry(points.type, points.coordinates);
        }

    }
}
