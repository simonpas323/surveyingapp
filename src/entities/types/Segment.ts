import Main from './Main';
import {Pole} from './Pole';
import {Geometry} from './Geometry';

export class Segment extends Main {

    poles: Array<Pole> = [];
    nazwa_linii: any;
    nazwa_ciagu_id: any;
    NAZWA_TAB: any;

    constructor(data: any = {poles: []}) {

        super(data);
        this.nazwa_linii = data.nazwa_linii ||'';
        this.nazwa_ciagu_id = data.nazwa_ciagu_id ||'';
        this.NAZWA_TAB = data.NAZWA_TAB ||'';
        if (data instanceof Segment) return data;
    }
}
