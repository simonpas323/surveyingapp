import {combineReducers} from 'redux';
import authReducer, {moduleName as authModule} from '../ducks/auth';
import mapReducer, {moduleName as mapModule} from '../ducks/map';
import adminReducer, {moduleName as adminModule} from '../ducks/admin';
import dialogReducer, {moduleName as dialogsModule} from '../ducks/dialogs';


export default combineReducers({
    [adminModule]: adminReducer,
    [authModule]: authReducer,
    [mapModule]: mapReducer,
    [dialogsModule]: dialogReducer,
});
